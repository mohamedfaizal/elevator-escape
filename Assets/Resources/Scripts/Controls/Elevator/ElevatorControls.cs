﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Elevator Controls
 Usage:
 Controls the whole "Elevator" scene. Attach it to an empty
 gameobject and set the intercom's position.

 Description:
 Controls the whole "Elevator" scene. It includes 'ElevatorInteraction'
 script, checking of the current state of the scene, audio management,
 level management, current state of the whole game (LevelHandler).

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/

public class ElevatorControls : MonoBehaviour {
    protected enum ELEVATOR_SCENE_STATE {
        ELEVATOR_SCENE_START,
        ELEVATOR_SCENE_STANDBY,
        ELEVATOR_SCENE_TOCORRIDOR,
        ELEVATOR_SCENE_UNLOADCORRIDOR,
        ELEVATOR_SCENE_LAST,
        ELEVATOR_SCENE_END,
        ELEVATOR_SCENE_DISABLE
    }

    public GameObject _Elevator;
    public Vector3 _intercomPos;
    public LightingInteraction _lightInteraction;
    public GameObject _closeDoorTriggerBox;

    public DelayedAppearanceBehaviour _girlAppearance;
    public GameObject _creepyGirlObject;

    public CablePanelBehaviour _cablePanel;

    private GameObject _SFXHolder;
    private ELEVATOR_SCENE_STATE _ELEVATOR_SCENE_STATE;

    // Cache
    private ElevatorInteraction _evelatorInteraction;
    private Fader _fader;
    private Vector3 _cameraPos;
    private TriggerPointsBehaviour _closeDoorTrigger;
    private const float f_time = 1.0f;

    private void Awake()
    {
        _ELEVATOR_SCENE_STATE = ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_START;
        _SFXHolder = null;

    // Cache definition
        _evelatorInteraction = _Elevator.GetComponent<ElevatorInteraction>();
        _fader = GameObject.FindObjectOfType<Fader>();
        _cameraPos = GameObject.Find("[Camera]").transform.position;
        _closeDoorTrigger = _closeDoorTriggerBox.GetComponent<TriggerPointsBehaviour>();
    }

    // Use this for initialization
    void Start () {
        _fader.ToFadeOut();
        AudioManager.GetInstance().StartVoice(_intercomPos);
    }
	
	// Update is called once per frame
	void Update () {
        switch (_ELEVATOR_SCENE_STATE) {
            case ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_START:
                ToStandby();
                break;

            case ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_STANDBY:
                ScriptIntroductionUpdate();
                break;

            case ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_TOCORRIDOR:
                CloseDoorToUnloadCorridor();
                break;

            case ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_UNLOADCORRIDOR:
                UnloadCorridorScene();
                break;

            case ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_LAST:
                ScriptLastUpdate();
                break;

            case ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_END:
                {
                    if (AudioManager.GetInstance().VoiceHasEnded()) {
                        _evelatorInteraction.ElevatorOpenDoorState();
                        GameObject _player = GameObject.FindGameObjectWithTag("Player");
                        _player.GetComponent<Rigidbody>().useGravity = true;
                        _ELEVATOR_SCENE_STATE = ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_DISABLE;
                    }
                }
                break;

            case ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_DISABLE:
                break;

            default:
                break;
        }
	}

    /*****************************************************************
     Used on Start.
     Sets the "Elevator" scene state to be on Standby mode.

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void ToStandby() {
        if (_fader.Standby) {
            _ELEVATOR_SCENE_STATE = ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_STANDBY;
            ScriptFlowManager.GetInstance().NextScriptFlow();
        }
    }

    /*****************************************************************
     The flow of the whole script for introduction. Most of it includes
     audio management, and level management.

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void ScriptIntroductionUpdate() {
        if (ScriptFlowManager.GetInstance().GetScriptFlowState() == ScriptFlowManager.SCRIPT_FLOW_STATE.SCRIPT_FLOW_INTRODUCTION)
        {
            switch (ScriptFlowManager.GetInstance().GetScriptIntroductionState())
            {
                case ScriptFlowManager.SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_START:
                    ScriptFlowManager.GetInstance().NextScriptIntroductionState();
                    break;

                case ScriptFlowManager.SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_INTERCOM_1:
                    {
                        if (AudioManager.GetInstance().VoiceHasEnded())
                        {
                            _SFXHolder = AudioManager.GetInstance().PlaySoundEffect("Intercom Buzz", _intercomPos);
                            _SFXHolder.GetComponent<SoundEffectHandler>().Customize(false, false);
                            ScriptFlowManager.GetInstance().NextScriptIntroductionState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_SOUNDEFFECT_1:
                    {
                        if (_SFXHolder.GetComponent<SoundEffectHandler>().HasEnded)
                        {
                            Destroy(_SFXHolder);
                            _SFXHolder = null;
                            Vector3 _tempPos = new Vector3(_cameraPos.x, _cameraPos.y, _cameraPos.z + 10.0f);
                            AudioManager.GetInstance().PlayNextVoice(_tempPos);
                            ScriptFlowManager.GetInstance().NextScriptIntroductionState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_UNKNOWN_1:
                    {
                        if (AudioManager.GetInstance().VoiceHasEnded())
                        {
                            _SFXHolder = AudioManager.GetInstance().PlaySoundEffect("Intercom Buzz", _intercomPos);
                            _SFXHolder.GetComponent<SoundEffectHandler>().Customize(false, false);
                            ScriptFlowManager.GetInstance().NextScriptIntroductionState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_SOUNDEFFECT_2:
                    {
                        if (_SFXHolder.GetComponent<SoundEffectHandler>().HasEnded)
                        {
                            Destroy(_SFXHolder);
                            _SFXHolder = null;
                            AudioManager.GetInstance().PlayNextVoice(_intercomPos);
                            StartCoroutine(LevelManager.GetInstance().LoadLevel("Corridor", false));
                            ScriptFlowManager.GetInstance().NextScriptIntroductionState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_INTERCOM_2:
                    {
                        if (AudioManager.GetInstance().VoiceHasEnded())
                        {                           
                            _evelatorInteraction.ElevatorOpenDoorState();
                            ScriptFlowManager.GetInstance().NextScriptIntroductionState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_END:
                    {
                        Destroy(_SFXHolder);
                        _SFXHolder = null;
                        if (_evelatorInteraction.OnStandby)
                        {
                            _lightInteraction.ToAnimationLightOff();
                            ScriptFlowManager.GetInstance().NextScriptIntroductionState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_DISABLE:
                    {
                        ScriptFlowManager.GetInstance().NextScriptFlow();
                        _ELEVATOR_SCENE_STATE = ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_TOCORRIDOR;
                    }
                    break;

                default:
                    break;
            }
        }
    }

    public void SwitchOnElevatorLights()
    {
        _lightInteraction.ToAnimationLightOnMax();
    }

    public void ActivateTriggerBox() {
        _closeDoorTriggerBox.SetActive(true);
    }

    public void CloseDoorToUnloadCorridor() {
        if (_closeDoorTriggerBox.activeSelf) {
            if (_closeDoorTrigger.Triggered)
            {
                _evelatorInteraction.ElevatorCloseDoorState();
                _ELEVATOR_SCENE_STATE = ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_UNLOADCORRIDOR;
            }
        }
    }

    private void UnloadCorridorScene() {
        if (_evelatorInteraction.OnStandby) {
            StartCoroutine(LevelManager.GetInstance().UnloadLevel("Elevator"));
            ScriptFlowManager.GetInstance().NextScriptFlow();
            _ELEVATOR_SCENE_STATE = ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_LAST;
        }
    }

    private void ScriptLastUpdate() {
        if (ScriptFlowManager.GetInstance().GetScriptFlowState() == ScriptFlowManager.SCRIPT_FLOW_STATE.SCRIPT_FLOW_LAST) {
            switch (ScriptFlowManager.GetInstance().GetScriptLastState()) {
                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_START:
                    AudioManager.GetInstance().PlayNextVoice(_intercomPos);
                    ScriptFlowManager.GetInstance().NextScriptLastState();
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_INTERCOM_1:
                    {
                        if (AudioManager.GetInstance().VoiceHasEnded()) {
                            _lightInteraction.ToAnimationFlicker();
                            // Elevator stall
                            AudioManager.GetInstance().PlaySoundEffect("SuddenElevatorStop", _Elevator.transform.position);
                            AudioManager.GetInstance().PlaySoundEffect("Intercom Buzzing", Vector3.zero);
                            ScriptFlowManager.GetInstance().NextScriptLastState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_ELEVATOR_STALL_SFX_1:
                    {
                        if (_lightInteraction.AnimationEnd) {
                            _lightInteraction.Restart();
                            _lightInteraction.ToAnimationLightOff();
                            AudioManager.GetInstance().PlayNextVoice(_intercomPos);
                            ScriptFlowManager.GetInstance().NextScriptLastState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_INTERCOM_2:
                    {
                        if (AudioManager.GetInstance().VoiceHasEnded()) {
                            AudioManager.GetInstance().PlayNextVoice(_intercomPos);
                            ScriptFlowManager.GetInstance().NextScriptLastState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_UNKNOWN_1:
                    {
                        if (AudioManager.GetInstance().VoiceHasEnded()) {
                            _SFXHolder = AudioManager.GetInstance().PlaySoundEffect("Intercom Buzz", _intercomPos);
                            ScriptFlowManager.GetInstance().NextScriptLastState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_SOUNDEFFECT_1:
                    {
                        if (_SFXHolder.GetComponent<SoundEffectHandler>().HasEnded) {
                            AudioManager.GetInstance().PlayNextVoice(_intercomPos);
                            ScriptFlowManager.GetInstance().NextScriptLastState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_INTERCOM_3:
                    {
                        if (AudioManager.GetInstance().VoiceHasEnded()) {
                            _SFXHolder = AudioManager.GetInstance().PlaySoundEffect("Siren", Vector3.zero);
                            ScriptFlowManager.GetInstance().NextScriptLastState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_SOUNDEFFECT_2:
                    {
                        if (_SFXHolder.GetComponent<SoundEffectHandler>().HasEnded) {
                            _fader.ToInstantFadeIn();
                            _lightInteraction.Restart();
                            _lightInteraction.ToAnimationLightOff();
                            GameObject _cameraEye = GameObject.FindGameObjectWithTag("MainCamera");
                            _Elevator.transform.localPosition = new Vector3(_Elevator.transform.position.x, _cameraEye.transform.position.y - 0.5f, -2);
                            _Elevator.transform.localEulerAngles = new Vector3(- 90f, 180f, 0f);
                            _SFXHolder = AudioManager.GetInstance().PlaySoundEffect("Boom", Vector3.zero);
                            ScriptFlowManager.GetInstance().NextScriptLastState();
                        }
                    }
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_END:
                    if (_SFXHolder.GetComponent<SoundEffectHandler>().HasEnded)
                    {
                        _lightInteraction.Restart();
                        _lightInteraction.ToAnimationFlickerContinued();
                        _fader.ToInstantFadeOut();
                        _creepyGirlObject.SetActive(true);
                        _cablePanel.GetComponent<BoxCollider>().enabled = true;
                        _cablePanel.gameObject.tag = "Interactables";
                        StartCoroutine(LevelManager.GetInstance().LoadLevel("Environment", false));
                        ScriptFlowManager.GetInstance().NextScriptLastState();
                    }
                    break;

                case ScriptFlowManager.SCRIPT_LAST.SCRIPT_LAST_DISABLE:
                    {
                        _cablePanel.ActivateGravity();
                        if (_girlAppearance.LookingAt)
                        {
                            _creepyGirlObject.GetComponent<AnimatorHandler>().ToWave();
                            _lightInteraction.Restart();
                            _lightInteraction.ToAnimationLightOnMax();
                            AudioManager.GetInstance().PlayNextVoice(_girlAppearance.gameObject.transform.position);
                            _ELEVATOR_SCENE_STATE = ELEVATOR_SCENE_STATE.ELEVATOR_SCENE_END;
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }
}