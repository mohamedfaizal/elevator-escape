﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorIndicator : MonoBehaviour {
    public Text _floorIndicatorText;
    public bool b_reached;

    private int i_floorLevel;
    private float f_floorMoveTimer;
    private const float f_floorTransitSpeed = 1.0f;
    private const int i_baseFloor = 1;
    private const int i_maxFloor = 10;
    private const string str_service = "Service";

    private void Awake() {
        b_reached = true;
        i_floorLevel = 1;
        f_floorMoveTimer = 0.0f;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void MoveIndicator(bool up) {
        if (!b_reached) {
            f_floorMoveTimer += f_floorTransitSpeed * Time.deltaTime;
            if (f_floorMoveTimer > f_floorTransitSpeed)
            {
                f_floorMoveTimer = 0.0f;
                if (!up)
                {
                    if (i_floorLevel - 1 > i_baseFloor)
                    {
                        i_floorLevel--;
                        _floorIndicatorText.text = i_floorLevel.ToString();
                    }

                    else
                    {
                        i_floorLevel = i_baseFloor;
                        _floorIndicatorText.text = i_floorLevel.ToString();
                        f_floorMoveTimer = 0.0f;
                        b_reached = true;
                    }
                }

                else
                {
                    if (i_floorLevel + 1 < i_maxFloor)
                    {
                        i_floorLevel++;
                        _floorIndicatorText.text = i_floorLevel.ToString();
                    }

                    else
                    {
                        i_floorLevel = i_baseFloor;
                        _floorIndicatorText.text = str_service;
                        f_floorMoveTimer = 0.0f;
                        b_reached = true;
                    }
                }
            }
        }
    }
}
