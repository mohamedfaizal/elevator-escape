﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/****************************************************************
 Lift Interaction
 Usage:
 Used for any lift interaction in any of the scenes. Attach this to
 the Lift GameObject for usage.

 Description:
 Any interaction that involves the lift and it's door.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/



public class ElevatorInteraction : MonoBehaviour {
    public enum ELEVATOR_STATE
    {
        ELEVATOR_STATE_STANDBY,
        ELEVATOR_STATE_READY,
        ELEVATOR_STATE_OPEN,
        ELEVATOR_STATE_CLOSE,
        ELEVATOR_STATE_MOVEUP,
        ELEVATOR_STATE_MOVEDOWN,
        ELEVATOR_STATE_STALL,
        ELEVATOR_STATE_END
    }

    public ButtonBehaviour _openElevatorDoor;
    public ButtonBehaviour _closeElevatorDoor;
    public GameObject _doorContainer;
    public GameObject _leftDoorSFXHolder;
    public GameObject _rightDoorSFXHolder;

    private ELEVATOR_STATE _ELEVATOR_STATE;
    public ELEVATOR_STATE GetElevatorState {
        get { return _ELEVATOR_STATE; }
    }

    public bool OnStandby {
        get { return _ELEVATOR_STATE == ELEVATOR_STATE.ELEVATOR_STATE_STANDBY; }
    }

    // Cache
    private ElevatorDoorBehaviour _elevatorDoorBehaviour;
    private FloorIndicator _floorIndicator;
    private SoundEffectHandler _leftDoorSFXHandler;
    private SoundEffectHandler _rightDoorSFXHandler;    

    private void Awake()
    {
        _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_STANDBY;
        _elevatorDoorBehaviour = _doorContainer.GetComponent<ElevatorDoorBehaviour>();
        _floorIndicator = GetComponent<FloorIndicator>();

        _leftDoorSFXHandler = _leftDoorSFXHolder.GetComponent<SoundEffectHandler>();
        _rightDoorSFXHandler = _rightDoorSFXHolder.GetComponent<SoundEffectHandler>();
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        switch (_ELEVATOR_STATE) {
            case ELEVATOR_STATE.ELEVATOR_STATE_STANDBY:
                break;

            case ELEVATOR_STATE.ELEVATOR_STATE_READY:
                break;

            case ELEVATOR_STATE.ELEVATOR_STATE_OPEN:
                OpenDoor();
                break;

            case ELEVATOR_STATE.ELEVATOR_STATE_CLOSE:
                CloseDoor();
                break;

            case ELEVATOR_STATE.ELEVATOR_STATE_MOVEUP:
                MoveElevatorUp();
                break;

            case ELEVATOR_STATE.ELEVATOR_STATE_MOVEDOWN:
                MoveElevatorDown();
                break;

            case ELEVATOR_STATE.ELEVATOR_STATE_STALL:
                break;

            case ELEVATOR_STATE.ELEVATOR_STATE_END:
                OpenDoor();
                break;
        }
    }

    private void OpenDoor() {
        _elevatorDoorBehaviour.ToOpenDoorState();

        if (_elevatorDoorBehaviour.DoorIsOpen) {
            _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_STANDBY;

            if (_elevatorDoorBehaviour.ElevatorDoorState != ELEVATOR_DOOR_STATE.EVELATOR_DOOR_STATE_STANDBY) {
                _elevatorDoorBehaviour.ToDoorStandby();
            }            
        }
    }

    private void CloseDoor() {
        _elevatorDoorBehaviour.ToCloseDoorState();

        if (!_elevatorDoorBehaviour.DoorIsOpen)
        {
            _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_STANDBY;

            if (_elevatorDoorBehaviour.ElevatorDoorState != ELEVATOR_DOOR_STATE.EVELATOR_DOOR_STATE_STANDBY) {
                _elevatorDoorBehaviour.ToDoorStandby();
            }

        }
    }

    private void MoveElevatorUp() {
        _floorIndicator.MoveIndicator(true);
    }

    private void MoveElevatorDown() {
        _floorIndicator.MoveIndicator(false);
    }

    public void ElevatorOpenDoorState() {
        _leftDoorSFXHandler.Play();
        _rightDoorSFXHandler.Play();
        _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_OPEN;
    }

    public void ElevatorCloseDoorState() {
        _leftDoorSFXHandler.Play();
        _rightDoorSFXHandler.Play();
        _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_CLOSE;
    }

    public void ElevatorMoveUp() {
        _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_MOVEUP;
        _floorIndicator.b_reached = false;
    }

    public void ElevatorMoveDown() {
        _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_MOVEDOWN;
        _floorIndicator.b_reached = false;
    }

    public void ElevatorStall() {
        _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_STALL;
    }

    public void ElevatorEnd() {
        _ELEVATOR_STATE = ELEVATOR_STATE.ELEVATOR_STATE_END;
        _elevatorDoorBehaviour.f_doorMovementTime = 5.0f;
    }
}
