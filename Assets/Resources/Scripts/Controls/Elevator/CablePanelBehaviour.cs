﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CablePanelBehaviour : MonoBehaviour {
    private ObjectInteraction _leftControllerInteraction;
    private ObjectInteraction _rightControllerInteraction;

    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start () {
        _leftControllerInteraction = GameObject.FindGameObjectWithTag("LeftController").GetComponent<ObjectInteraction>();
        _rightControllerInteraction = GameObject.FindGameObjectWithTag("RightController").GetComponent<ObjectInteraction>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivateGravity() {
        if ((_leftControllerInteraction != null && _leftControllerInteraction.CollidingObjectName == this.gameObject.name) ||
            (_rightControllerInteraction != null && _rightControllerInteraction.CollidingObjectName == this.gameObject.name)) {
            GetComponent<Rigidbody>().useGravity = true;
        }
    }
}
