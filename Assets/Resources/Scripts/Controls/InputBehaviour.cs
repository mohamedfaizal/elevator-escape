﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Input behaviour
 Usage:
 Used for HTC Vive's controllers. Attach this script to the
 controller's component.

 Description:
 Tracks buttons and axises.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/

public class InputBehaviour : MonoBehaviour {
    private SteamVR_TrackedObject _TrackedObject;

    public bool b_isGrabbing;

    /*****************************************************************
     Getter for the controller device.
     Tracks the controller's input and any other sensory motions.

     Return Type: SteamVR_Controller.Device
    *****************************************************************/
    public SteamVR_Controller.Device Controller {
        get { return SteamVR_Controller.Input((int)_TrackedObject.index); }
    }

    private void Awake()
    {
        _TrackedObject = GetComponent<SteamVR_TrackedObject>();
        b_isGrabbing = false;
    }

    private void Update()
    {
        
    }

    /*****************************************************************
     Returns the axis of the controller's touchpad the player is
     currently touching on.

     Return Type: Vector2
    *****************************************************************/
    public Vector2 ControllerGetAxis()
    {
        return Controller.GetAxis();
    }

    /*****************************************************************
     Returns if the touchpad is pressed down.

     Return Type: bool
    *****************************************************************/
    public bool TouchPadDown()
    {
        return Controller.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad);
    }

    /*****************************************************************
     Returns if the touchpad is released.

     Return Type: bool
    *****************************************************************/
    public bool TouchPadUp()
    {
        return Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad);
    }

    /*****************************************************************
    Returns if the touchpad is pressed and held.

    Return Type: bool
    *****************************************************************/
    public bool TouchPadPress()
    {
        return Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad);
    }

    /*****************************************************************
     Returns if the hair trigger is pressed and held.

     Return Type: bool
    *****************************************************************/
    public bool HairTriggerPress()
    {
        return Controller.GetHairTrigger();
    }

    /*****************************************************************
     Returns if the hair trigger is pressed.

     Return Type: bool
     Parameter: void
    *****************************************************************/
    public bool HairTriggerDown()
    {
        return Controller.GetHairTriggerDown();
    }

    /*****************************************************************
     Returns if the hair trigger is released.

     Return Type: bool
    *****************************************************************/
    public bool HairTriggerUp()
    {
        return Controller.GetHairTriggerUp();
    }

    /*****************************************************************
     Returns if the grip is pressed.

     Return Type: bool
    *****************************************************************/
    public bool GripDown()
    {
        return Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip);
    }

    /*****************************************************************
     Returns if the grip is released.

     Return Type: bool
    *****************************************************************/
    public bool GripUp()
    {
        return Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip);
    }
}
