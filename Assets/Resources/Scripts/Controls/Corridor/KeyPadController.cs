﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 KeyPad Controller:
 Manages button and password for Corridor scene

 Noel Ho Sing Nam
 noelhosingnam@hotmail.com/ noelhosingnam@viziofly.com
 +65-86111251
 ****************************************************************/

public class KeyPadController : MonoBehaviour {
    public List<ButtonBehaviour> _passwordButtons = new List<ButtonBehaviour>();
    private List<bool> _passwordButtonsPushed = new List<bool>();
    private Queue<int> _keyInputList = new Queue<int>();
    private List<int> _passwordCombinationInt = new List<int>();
    private int i_maxKeyInputListStorage = 1;
    public int i_maxNumberofKeys = 3;
    public string s_passwordCombination;
    public bool b_matchFound = false;
    public GameObject _panel;

    // Use this for initialization
    void Start()
    {
        // Searches the string for the first i_maxNumberofKeys possible numbers
        for (int i = 0; i < i_maxNumberofKeys && i < s_passwordCombination.Length; i++)
        {
            if(s_passwordCombination[i] - 48 > _passwordButtons.Count || s_passwordCombination[i] <= 48)
            {
                i_maxNumberofKeys++;
                continue;
            }
            _passwordCombinationInt.Add(s_passwordCombination[i] - 48);
        }

        // Increases the size of key logs, while ensuring it is the minimum size
        if (i_maxKeyInputListStorage < _passwordCombinationInt.Count)
        {
            i_maxKeyInputListStorage = _passwordCombinationInt.Count;
        }

        // Makes button one time push
        for (int i = 0; i < _passwordButtons.Count; i++)
        {
            _passwordButtonsPushed.Add(false);
        }
    }

    // Update is called once per frame
    void Update () {

        // Deletes the oldest key input to reduce memory storage
        if (_keyInputList.Count > i_maxKeyInputListStorage)
        {
            _keyInputList.Dequeue();
        }

        // Checks for button press
        for(int i = 0; i < _passwordButtons.Count; i++)
        {
            if(_passwordButtons[i].b_buttonPressed == true)
            {
                if (_passwordButtonsPushed[i] == false)
                {
                    _passwordButtonsPushed[i] = true;
                    _keyInputList.Enqueue(i + 1);
                    CheckCombination();
                }
            }
            else
            {
                _passwordButtonsPushed[i] = false;
            }
        }
    }

    // Checks if it matches any password combinations
    private bool CheckCombination()
    {

        // Ignore combination if player presses less buttons than combination length
        if (_keyInputList.Count < _passwordCombinationInt.Count)
        {
            return false;
        }

        // Checks individual numbers in password combination
        for (int j = 0; j <= _passwordCombinationInt.Count; j++)
        {
            // Combination i matches exactly
            if (j == _passwordCombinationInt.Count)
            {
                b_matchFound = true;
                CombinationFound();
                return true;
            }

            // Compares the key logs from the latest input to the password combination i from the last number
            if (_keyInputList.ToArray()[_keyInputList.Count - 1 - j] == _passwordCombinationInt[_passwordCombinationInt.Count - 1 - j])
            {
                continue;
            }
            else
            {
                // Combination does not match
                return false;
            }
        }
        return false;
    }

    // Combination matches password
    private void CombinationFound()
    {
        _panel.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }
}
