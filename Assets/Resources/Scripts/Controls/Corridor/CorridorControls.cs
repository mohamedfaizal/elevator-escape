﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorridorControls : MonoBehaviour {
    public enum CORRIDOR_SCENE_STATE {
        CORRIDOR_SCENE_STATE_START,
        CORRIDOR_SCENE_STATE_STANDBY,
        CORRIDOR_SCENE_STATE_PUZZLE,
        CORRIDOR_SCENE_STATE_DOLL,
        CORRIDOR_SCENE_STATE_BRANCH,
        CORRIDOR_SCENE_STATE_END,
        CORRIDOR_SCENE_STATE_DISABLE
    }

    public enum CORRIDOR_SCENE_BRANCH {
        CORRIDOR_SCENE_BRANCH_START,
        CORRIDOR_SCENE_BRANCH_1_1,
        CORRIDOR_SCENE_BRANCH_1_2,
        CORRIDOR_SCENE_BRANCH_2,
        CORRIDOR_SCENE_BRANCH_END,
        CORRIDOR_SCENE_BRANCH_DISABLE
    }

    public KeyPanelBehaviour _keyPanel;
    public RandomSFXController _randomSFXController;
    
    public AnimatorHandler _girl;

    public GameObject _tensionBuildUp;

    [Range(10f, 20f)]
    [Tooltip("Sets the idle time if the player takes too long for an action.")]
    public float f_idleTime = 15.0f;
    [Range(30f, 120f)]
    [Tooltip("Sets the puzzle solving time.")]

    public float f_puzzleSolverTime = 30.0f;
    public LightingInteraction _lightInteraction;

    private CORRIDOR_SCENE_STATE _CORRIDOR_SCENE_STATE;
    private CORRIDOR_SCENE_BRANCH _CORRIDOR_SCENE_BRANCH;
    private CORRIDOR_SCENE_BRANCH _CORRIDOR_SCENE_BRANCHTRANSITION;
    private CORRIDOR_SCENE_BRANCH _transitionHolder;

    public GameObject _triggerPointOne;
    private float f_timer;

    private float f_lightsDelayTimer;
    private bool b_lightOnTransit;
    private bool b_branchTransitFinish;
    private bool b_tensionBuildUpDone;
    private bool b_dollGrabbed;

    // Cache
    private TriggerPointsBehaviour _pointOneTrigger;
    private InputBehaviour _leftController;
    private InputBehaviour _rightController;
    private Vector3 _intercomPos;
    private Camera _cameraEye;
    private BGMHandler _tensionBuildUpAudio;
    private ElevatorControls _elevatorControls;

    private const float f_time = 1.0f;
    private const float f_lightsDelayTime = 2.0f;

    private void Awake()
    {
        _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_START;
        _CORRIDOR_SCENE_BRANCH = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_START;
        _CORRIDOR_SCENE_BRANCHTRANSITION = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_START;
        f_timer = 0.0f;
        b_branchTransitFinish = false;
        b_tensionBuildUpDone = false;
        b_dollGrabbed = false;
        f_lightsDelayTimer = 0.0f;
        b_lightOnTransit = false;
    }

    // Use this for initialization
    void Start () {
        // Cache
        _pointOneTrigger = _triggerPointOne.GetComponent<TriggerPointsBehaviour>();
        _leftController = GameObject.FindGameObjectWithTag("LeftController").GetComponent<InputBehaviour>();
        _rightController = GameObject.FindGameObjectWithTag("RightController").GetComponent<InputBehaviour>();
        _cameraEye = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        _tensionBuildUpAudio = _tensionBuildUp.GetComponent<BGMHandler>();
        _elevatorControls = GameObject.FindObjectOfType<ElevatorControls>();
        _intercomPos = _elevatorControls._intercomPos;
    }

    // Update is called once per frame
    void Update () {
        switch (_CORRIDOR_SCENE_STATE) {
            case CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_START:
                if (ScriptFlowManager.GetInstance().ScriptIntroductionDisable()) {
                    AudioManager.GetInstance().AssignBackgroundMusic("TutorialBGM2", true, true);
                    AudioManager.GetInstance().PlayNextVoice(_intercomPos);
                    _randomSFXController.StartNewSoundEffect();
                    _lightInteraction.ToAnimationStatic();
                    _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_STANDBY;
                }
                break;
            
                // Changes state when the player is near the key panel
            case CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_STANDBY:
                {
                    if (_randomSFXController.RandomSoundEffectEnded())
                    {
                        _randomSFXController.PlayVoice();
                    }

                    if (!_pointOneTrigger.Triggered) {
                        if (AudioManager.GetInstance().VoiceHasEnded())
                            AudioManager.GetInstance().RepeatVoice(5f);
                    }


                    else if (_pointOneTrigger.Triggered) {
                        if (AudioManager.GetInstance().VoiceHasEnded()) {
                            AudioManager.GetInstance().PlayNextVoiceDelayed(2f);
                            _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_PUZZLE;
                            f_timer = 0.0f;
                        }
                    }               
                }
                break;

                // If player fails to solve the puzzle, to branch 2 stage
                // Else if player successfully solves the puzzle, to branch 1 stage
            case CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_PUZZLE:
                {
                    f_timer += f_time * Time.deltaTime;
                    if (f_timer > f_puzzleSolverTime - _tensionBuildUpAudio.GetAudioClipLength()) {
                        if (!b_tensionBuildUpDone) {
                            b_tensionBuildUpDone = true;
                            _tensionBuildUpAudio.Play();
                        }
                    }

                    if (f_timer < f_puzzleSolverTime) {
                        if (_keyPanel.MatchIsFound) {
                            f_timer = 0f;
                            _keyPanel.OpenServicePanel();
                            _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_DOLL;
                            f_timer = 0.0f;
                        }
                    }

                    else {
                        f_timer = 0f;
                        _lightInteraction.Restart();
                        _lightInteraction.ToAnimationGradualLightOff();
                        _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_BRANCH;
                        _CORRIDOR_SCENE_BRANCHTRANSITION = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_2;
                        f_timer = 0.0f;
                    }
                }
                break;

                // If the player grabs the doll, to branch 1.1 stage.
                // Else if player fails to grab the doll, to 1.2 stage.
            case CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_DOLL:
                {
                    f_timer += f_time * Time.deltaTime;
                    if (f_timer > f_idleTime - _tensionBuildUpAudio.GetAudioClipLength() && !b_dollGrabbed) {
                        if (!b_tensionBuildUpDone) {
                            b_tensionBuildUpDone = true;
                            _tensionBuildUpAudio.Play();
                        }
                    }

                    if (f_timer < f_idleTime) {
                        if ((_leftController != null && _leftController.b_isGrabbing) || (_rightController != null && _rightController.b_isGrabbing) || Input.GetKeyUp(KeyCode.A)) {
                            b_dollGrabbed = true;                                                
                        }

                        if (b_dollGrabbed) {
                            f_lightsDelayTimer += f_time * Time.deltaTime;
                            _girl.gameObject.SetActive(true);
                            _lightInteraction.Restart();
                            _lightInteraction.ToAnimationLightOff();

                            if (f_lightsDelayTimer > f_lightsDelayTime)
                            {
                                f_timer = 0f;
                                f_lightsDelayTimer = 0f;
                                _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_BRANCH;
                                _CORRIDOR_SCENE_BRANCHTRANSITION = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_1;
                            }
                        }
                    }

                    else {
                        f_timer = 0f;
                        _lightInteraction.Restart();
                        _lightInteraction.ToAnimationGradualLightOff();
                        _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_BRANCH;
                        _CORRIDOR_SCENE_BRANCHTRANSITION = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_2;
                    }
                }
                break;

            case CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_BRANCH:
                BranchingEventUpdate();
                break;

            case CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_END:
                if (AudioManager.GetInstance().VoiceHasEnded()) {
                    _elevatorControls.ActivateTriggerBox();
                    _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_DISABLE;
                }
                break;

            case CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_DISABLE:
                break;

            default:
                break;
        }
	}

    private void BranchingEventUpdate() {
        switch (_CORRIDOR_SCENE_BRANCH) {
            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_START:
                TransitionToBranchEventUpdate();
                if (b_branchTransitFinish) {
                    _CORRIDOR_SCENE_BRANCH = _transitionHolder;
                }
                    
                break;

            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_1:
                {
                    _lightInteraction.ToAnimationFlicker();
                    _CORRIDOR_SCENE_BRANCH = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_END;
                }
                break;

            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_2:
                _lightInteraction.ToAnimationGradualLightOn();
                _CORRIDOR_SCENE_BRANCH = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_END;
                break;

            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_2:
                _lightInteraction.ToAnimationGradualLightOn();
                _CORRIDOR_SCENE_BRANCH = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_END;
                break;

            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_END:
                {
                    switch (_transitionHolder) {
                        case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_1:
                            {
                                if (_lightInteraction.AnimationEnd) {
                                    AudioManager.GetInstance().PlayNextVoice("Branch 1_1", _intercomPos, "Branch 2");
                                    _elevatorControls.SwitchOnElevatorLights();
                                    _lightInteraction.Restart();
                                    _lightInteraction.ToAnimationLightOn();
                                    _CORRIDOR_SCENE_BRANCH = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_DISABLE;
                                }

                            }
                            break;

                        case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_2:
                            {
                                AudioManager.GetInstance().PlayNextVoice("Branch 1_2", _intercomPos, "Branch 2");
                                _elevatorControls.SwitchOnElevatorLights();
                                _CORRIDOR_SCENE_BRANCH = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_DISABLE;
                            }
                            break;

                        case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_2:
                            {
                                AudioManager.GetInstance().PlayNextVoice("Branch 2", _intercomPos, "Branch 2");
                                _elevatorControls.SwitchOnElevatorLights();
                                _CORRIDOR_SCENE_BRANCH = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_DISABLE;
                            }
                            break;

                        default:
                            break;
                    }
                }
                break;

            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_DISABLE:
                _CORRIDOR_SCENE_STATE = CORRIDOR_SCENE_STATE.CORRIDOR_SCENE_STATE_END;
                break;

            default:
                break;
        }
    }

    private void TransitionToBranchEventUpdate() {
        switch (_CORRIDOR_SCENE_BRANCHTRANSITION) {
            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_1:
                {
                    _transitionHolder = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_1;
                    Vector3 cameraView = new Vector3(0.5f, 0.5f, 0);
                    Ray ray = _cameraEye.ViewportPointToRay(cameraView);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit, 0.5f)) {
                        if (hit.collider.gameObject.tag == "Girl") {
                            _lightInteraction.Restart();
                            _lightInteraction.ToAnimationLightOnMax();
                            _girl.ToRunning();
                            AudioManager.GetInstance().PlaySoundEffect("Loud Boom", _girl.transform.position);
                            _CORRIDOR_SCENE_BRANCHTRANSITION = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_END;
                        }
                    }
                }
                break;

            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_2:
                {
                    _transitionHolder = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_1_2;
                    if (_lightInteraction.AnimationEnd) {
                        _girl.gameObject.SetActive(true);

                        Vector3 cameraView = new Vector3(0.5f, 0.5f, 0);
                        Ray ray = _cameraEye.ViewportPointToRay(cameraView);
                        RaycastHit hit;

                        if (Physics.Raycast(ray, out hit, 0.5f))
                        {
                            if (hit.collider.gameObject.tag == "Girl")
                            {
                                _lightInteraction.Restart();
                                _lightInteraction.ToAnimationLightOnMax();
                                _girl.ToRunning();
                                AudioManager.GetInstance().PlaySoundEffect("Loud Boom", _girl.transform.position);
                                _CORRIDOR_SCENE_BRANCHTRANSITION = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_END;
                            }
                        }
                    }
                }
                break;

                // If the player fails to get the password on time.
            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_2:
                {
                    _transitionHolder = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_2;
                    if (_lightInteraction.AnimationEnd) {
                        _girl.gameObject.SetActive(true);
                    }

                    Vector3 cameraView = new Vector3(0.5f, 0.5f, 0);
                    Ray ray = _cameraEye.ViewportPointToRay(cameraView);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit, 0.5f))
                    {
                        if (hit.collider.gameObject.tag == "Girl")
                        {
                            _lightInteraction.Restart();
                            _lightInteraction.ToAnimationLightOnMax();
                            _girl.ToRunning();
                            AudioManager.GetInstance().PlaySoundEffect("Loud Boom", _girl.transform.position);
                            _CORRIDOR_SCENE_BRANCHTRANSITION = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_END;
                        }
                    }
                }
                break;

            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_END:
                {
                    if (_girl.AnimationEnd)
                    {
                        b_lightOnTransit = true;

                        _lightInteraction.ToAnimationLightOff();
                        _girl.Restart();
                        _girl.gameObject.SetActive(false);
                    }
                    if (b_lightOnTransit) {
                        f_lightsDelayTimer += f_time * Time.deltaTime;
                        if (f_lightsDelayTimer > f_lightsDelayTime)
                        {
                            AudioManager.GetInstance().StopBackgroundMusic();
                            _lightInteraction.Restart();
                            f_lightsDelayTimer = 0f;
                            b_lightOnTransit = false;
                            _CORRIDOR_SCENE_BRANCHTRANSITION = CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_DISABLE;
                        }
                    }               
                }
                break;

            case CORRIDOR_SCENE_BRANCH.CORRIDOR_SCENE_BRANCH_DISABLE:
                {
                    b_branchTransitFinish = true;
                }
                break;

            default:
                break;
        }
    }
}
