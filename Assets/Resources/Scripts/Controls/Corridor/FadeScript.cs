﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeScript : MonoBehaviour {

    public Component[] renderers;

    private void Start()
    {
        renderers = GetComponentsInChildren<Renderer>();
    }

    // Update is called once per frame
    void Update () {
        foreach (Renderer texture in renderers)
        {
            if (texture.material.color.a > 0.00f)
            {
                Color currentColor = texture.material.color;
                texture.material.SetFloat("_Mode", 2);
                texture.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, currentColor.a - 0.01f);
                texture.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                texture.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                texture.material.DisableKeyword("_ALPHATEST_ON");
                texture.material.EnableKeyword("_ALPHABLEND_ON");
                texture.material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                texture.material.renderQueue = 3000;
            }
        }
    }
}
