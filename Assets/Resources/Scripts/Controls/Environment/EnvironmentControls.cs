﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentControls : MonoBehaviour {
    public GameObject _environment;
    private GameObject _cameraContainer;
    private Fader _fader;
    private bool b_hitGround;

    private float f_timer;
    private const float f_transitTime = 2f;
    private const float f_time = 1.0f;
	// Use this for initialization
	void Start () {
        _fader = GameObject.FindGameObjectWithTag("Fader").GetComponent<Fader>();
        _cameraContainer = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if (b_hitGround)
        {
            f_timer += f_time * Time.deltaTime;
            if (f_timer > f_transitTime)
            {
                _fader.ToFadeOut();
                b_hitGround = false;
                StartCoroutine(LevelManager.GetInstance().LoadLevel("Credits", true));
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TriggerCollider") {
            _cameraContainer.GetComponent<Rigidbody>().useGravity = false;
            _cameraContainer.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
            _cameraContainer.GetComponent<Rigidbody>().angularVelocity = new Vector3(0f, 0f, 0f);
            _fader.ToInstantFadeIn();
            AudioManager.GetInstance().InstantiateSoundEffect("Boom", _cameraContainer.transform.position);
            _environment.SetActive(false);
            b_hitGround = true;
        }
    }
}
