﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Tutorial Controls Script:
 Manages the event order for the tutorial room

 Noel Ho Sing Nam
 noelhosingnam@hotmail.com/ noelhosingnam@viziofly.com
 +65-86111251
 ****************************************************************/

public class TutorialControls : MonoBehaviour {

    enum TUTORIAL_EVENTS
    {
        TUTORIAL_EVENTS_START,
        TUTORIAL_EVENTS_STEP1,
        TUTORIAL_EVENTS_STEP2,
        TUTORIAL_EVENTS_STEP3,
        TUTORIAL_EVENTS_END
    }
    public GameObject _ballSpawner;
    public GameObject _ball;

    public ButtonBehaviour _ballSpawn_button;
    public GameObject _gameStart_button;
    private GameObject _controller_left;
    private GameObject _controller_right;

    // Tutorial Sign Displays
    // Step 1
    public GameObject _lookToRight;
    public GameObject _trackPad;
    // Step 2
    public GameObject _lookBehind;
    public GameObject _placeBall;
    // Step 3
    public GameObject _lookToLeft;
    public GameObject _beginGame;

    private Fader _fader;
    private bool b_readyToChangeScene;

    // Caches
    private InputBehaviour _leftControllerBehaviour;
    private InputBehaviour _rightControllerBehaviour;

    TUTORIAL_EVENTS _TUTORIAL_EVENTS;

    private void Awake()
    {
        _fader = FindObjectOfType<Fader>();
        _controller_left = GameObject.FindGameObjectWithTag("LeftController");
        _controller_right = GameObject.FindGameObjectWithTag("RightController");

        if (_controller_left != null) {
            _leftControllerBehaviour = _controller_left.GetComponent<InputBehaviour>();
        }

        if (_controller_right != null) {
            _rightControllerBehaviour = _controller_right.GetComponent<InputBehaviour>();
        }

        b_readyToChangeScene = true;
    }

    /*****************************************************************
     Use this for initialization

     Return Type: void
     Parameter: void
    *****************************************************************/
    void Start () {
        _lookToRight.SetActive(false);
        _trackPad.SetActive(false);
        _lookBehind.SetActive(false);
        _placeBall.SetActive(false);
        _lookToLeft.SetActive(false);
        _beginGame.SetActive(false);

        _gameStart_button.SetActive(false);

        if (LevelManager.GetInstance().SceneIsLoaded())
            _fader.ToFadeOut();
    }

    /*****************************************************************
     Update is called once per frame

     Return Type: void
     Parameter: void
    *****************************************************************/
    void Update () {

        if (Input.GetKeyUp(KeyCode.U)) {
            _TUTORIAL_EVENTS = TUTORIAL_EVENTS.TUTORIAL_EVENTS_STEP3;
        }
		switch(_TUTORIAL_EVENTS)
        {
            case TUTORIAL_EVENTS.TUTORIAL_EVENTS_START:
            {
                    if(_ballSpawn_button.b_buttonPressed == true)
                    {
                        StepOneTrigger();
                    }
            }
            break;

            case TUTORIAL_EVENTS.TUTORIAL_EVENTS_STEP1:
            {
                if (CheckIfBallHold() || Input.GetKey(KeyCode.U))
                {
                        StepTwoTrigger();
                }

                if (_ballSpawn_button.b_buttonPressed == true)
                {
                        SpawnBall();
                }
            }
            break;

            case TUTORIAL_EVENTS.TUTORIAL_EVENTS_STEP2:
            {
                if (_ballSpawn_button.b_buttonPressed == true)
                {
                    SpawnBall();
                }
            }
            break;

            case TUTORIAL_EVENTS.TUTORIAL_EVENTS_STEP3:
            {
                if(_gameStart_button.GetComponent<ButtonBehaviour>().b_buttonPressed == true)
                {
                    EndTrigger();
                }
            }
            break;

            case TUTORIAL_EVENTS.TUTORIAL_EVENTS_END:
                NextSceneTrigger();
                break;

        }
	}

    /*****************************************************************
     Checks if player is holding the ball

     Return Type: bool
     Parameter: void
    *****************************************************************/
    public bool CheckIfBallHold()
    {
        bool holding = false;

        if (_controller_left != null)
        {
            if(_leftControllerBehaviour.b_isGrabbing)
                holding = true;
        }
        if (_controller_right != null) 
        {
            if (_rightControllerBehaviour.b_isGrabbing)
                holding = true;
        }

        return holding;
    }

    /*****************************************************************
     Triggers step one of events

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void StepOneTrigger()
    {
        _lookToRight.SetActive(true);
        _trackPad.SetActive(true);
        SpawnBall();
        AudioManager.GetInstance().InstantiateSoundEffect("Boom", _ballSpawner.transform.position);
        _TUTORIAL_EVENTS = TUTORIAL_EVENTS.TUTORIAL_EVENTS_STEP1;
    }

    /*****************************************************************
     Trigger step two of events

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void StepTwoTrigger()
    {
        _lookBehind.SetActive(true);
        _placeBall.SetActive(true);
        AudioManager.GetInstance().InstantiateSoundEffect("Look behind you", _placeBall.transform.position);
        _TUTORIAL_EVENTS = TUTORIAL_EVENTS.TUTORIAL_EVENTS_STEP2;
    }

    /*****************************************************************
     Trigger step three of events

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void StepThreeTrigger()
    {
        _lookToLeft.SetActive(true);
        _beginGame.SetActive(true);
        _gameStart_button.SetActive(true);
        AudioManager.GetInstance().InstantiateSoundEffect("Sinister Laughter", _gameStart_button.transform.position);
        _TUTORIAL_EVENTS = TUTORIAL_EVENTS.TUTORIAL_EVENTS_STEP3;
    }

    /*****************************************************************
     Trigger the end stage of events

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void EndTrigger()
    {
        _TUTORIAL_EVENTS = TUTORIAL_EVENTS.TUTORIAL_EVENTS_END;
        AudioManager.GetInstance().StartBGMGradualFadeOff();
        _fader.ToFadeIn();
    }

    /*****************************************************************
     Triggers the next scene

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void NextSceneTrigger() {
        if (_fader.Standby && b_readyToChangeScene && AudioManager.GetInstance().GradualStopBackgroundMusic(1.0f)) {
            b_readyToChangeScene = false;
            StartCoroutine(LevelManager.GetInstance().LoadLevel("Pregame", true));
        }
    }

    /*****************************************************************
     Spawns the ball / resets it's position if it's not active

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void SpawnBall()
    {
        if(_ball)
        {
            if (!_ball.activeSelf)
            {
                _ball.SetActive(true);
            }
            else
            {
                _ball.transform.position = _ballSpawner.transform.position;
                _ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
    }
}
