﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialBallSlot : MonoBehaviour {

    public TutorialControls _TutorialControls;

    private void OnTriggerStay(Collider collision)
    {
        if(collision.gameObject.name == "Ball")
        {
            if (!_TutorialControls.CheckIfBallHold())
            {
                Destroy(collision.gameObject);
                gameObject.transform.GetChild(0).gameObject.SetActive(true);

                _TutorialControls.StepThreeTrigger();
            }
        }
    }
}
