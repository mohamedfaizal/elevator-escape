﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSFXController : MonoBehaviour {
    [Tooltip("Set the max range of z Axis. E.g.: Range 10 will have a range of - 10 to 10")]
    public float f_randomForwardRange;
    [Tooltip("Set the max range of y Axis. E.g.: Range 10 will have a range of - 10 to 10")]
    public float f_randomUpRange;
    [Tooltip("Set the max range of x Axis. E.g.: Range 10 will have a range of - 10 to 10")]
    public float f_randomRightRange;

    [Tooltip("Attach the audioclips that you want to play during runtime. \n" +
        "It will play in random sequence.")]
    public List<AudioClip> _randomedAudioClip;

    [Range(0f, 3f)]
    [Tooltip("Delay time to play the next random SFX")]
    private float f_pause = 3f;

    public GameObject _randomSFXObject;
    private int i_totalAudioCount;
    private Vector3 _randomPosition;
    
    private float f_timer;

    // Cache
    private SoundEffectHandler _audio;

    private void Awake()
    {
        i_totalAudioCount = 0;
        f_timer = 0f;
    }

    // Use this for initialization
    void Start()
    {
        if (f_randomForwardRange < 0.0f)
            f_randomForwardRange = Mathf.Abs(f_randomForwardRange);

        if (f_randomUpRange < 0.0f)
            f_randomUpRange = Mathf.Abs(f_randomUpRange);

        if (f_randomRightRange < 0.0f)
            f_randomRightRange = Mathf.Abs(f_randomRightRange);

        i_totalAudioCount = _randomedAudioClip.Count;
        _audio = _randomSFXObject.GetComponent<SoundEffectHandler>();

        int i = Random.Range(0, i_totalAudioCount);
        _audio.AssignAudioClip(_randomedAudioClip[i]);
    }

    // Update is called once per frame
    void Update()
    {
        if (_audio.HasEnded) {
            f_timer += 1.0f * Time.deltaTime;
        }
    }

    public void StartNewSoundEffect()
    {
        int i = Random.Range(0, i_totalAudioCount);
        _audio.AssignAudioClip(_randomedAudioClip[i]);
        _audio.Play();
    }

    public Vector3 GetNewRandomPosition()
    {
        _randomPosition = new Vector3(Random.Range(-f_randomRightRange, f_randomRightRange),
            Random.Range(-f_randomUpRange, f_randomUpRange),
            Random.Range(-f_randomForwardRange, f_randomForwardRange));

        return _randomPosition;
    }

    public bool RandomSoundEffectEnded()
    {
        return _audio.HasEnded;
    }

    public void PlayVoice()
    {
        if (f_timer > f_pause) {
            _randomSFXObject.transform.position = new Vector3(GetNewRandomPosition().x, GetNewRandomPosition().y, GetNewRandomPosition().z);
            int i = Random.Range(0, i_totalAudioCount);
            _audio.AssignAudioClip(_randomedAudioClip[i]);
            _audio.Play();
            f_timer = 0f;
        }

    }

    public void Stop()
    {
        _audio.Stop();
    }
}
