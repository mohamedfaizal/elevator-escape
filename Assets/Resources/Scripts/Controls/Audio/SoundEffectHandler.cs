﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectHandler : MonoBehaviour {
    [Tooltip("Destroys this object after SFX finished playing")]
    public bool b_destroyOnFinish = true;
    [Tooltip("Is this repeatable? Not applicable if object destroys on finish")]
    public bool b_isRepeatable = false;
    [Tooltip("Attach this object's AudioSource here")]
    public AudioSource _thisAudioSource;

    private float f_length;
    private const float f_seconds = 1.0f;
    public float GetLength {
        get { return f_length; }
    }
    private bool b_ended;
    public bool HasEnded {
        get { return b_ended; }
    }

    private void Awake() {
        b_ended = false;
    }

    // Use this for initialization
    void Start() {
        if (b_isRepeatable) {
            if (b_destroyOnFinish) {
                b_isRepeatable = false;
            }
        }

        if (_thisAudioSource.clip != null)
            f_length = _thisAudioSource.clip.length;
    }

    public void Customize(bool destroyOnFinish, bool repeatable) {
        b_destroyOnFinish = destroyOnFinish;
        if (repeatable) {
            if (b_destroyOnFinish) {
                b_isRepeatable = false;
            }
        }
        else
            b_isRepeatable = repeatable;
    }
	
	// Update is called once per frame
	void Update () {
        if (!b_ended)
        {
            f_length -= f_seconds * Time.deltaTime;
        }

        else {
            if (b_destroyOnFinish)
            {
                Destroy(gameObject);
            }
        }

        if (f_length <= 0f)
        {
            b_ended = true;
            f_length = 0f;
        }
	}

    public void Play() {
        if (b_ended)
        {
            b_ended = false;
            f_length = _thisAudioSource.clip.length;
            _thisAudioSource.Play();
        }
    }

    public void AssignAudioClip(AudioClip clip) {
        _thisAudioSource.clip = clip;
        f_length = clip.length;
    }

    public void Play(bool bypass, bool delay, ulong delayTime = 0) {
        if (!bypass) {
            if (f_length < 0f) {
                if (delay)
                    _thisAudioSource.Play(delayTime);

                else
                    _thisAudioSource.Play();

                b_ended = false;
            }
        }

        else {
            if (_thisAudioSource.isPlaying)
                _thisAudioSource.Stop();

            if (delay)
                _thisAudioSource.Play(delayTime);

            else
                _thisAudioSource.Play();

            b_ended = false;
        }
    }

    public void Stop()
    {
        _thisAudioSource.Stop();
        f_length = 0f;
        b_ended = true;
    }

    private void OnDestroy() {
        _thisAudioSource = null;
    }
}
