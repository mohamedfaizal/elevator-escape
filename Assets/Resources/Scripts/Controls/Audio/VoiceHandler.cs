﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceHandler : MonoBehaviour {
    public AudioSource _thisAudioSource;
    private bool b_ended;
    private bool b_changeOnEnd;
    private float f_length;
    private const float f_seconds = 1.0f;

    public bool HasEnded {
        get { return b_ended; }
    }

    public float BGMLength {
        get { return f_length; }
        set { b_ended = false; f_length = value; }
    }

    private void Awake() {
        b_ended = false;
        f_length = 0.0f;
    }

    // Use this for initialization
    void Start () {
        if (_thisAudioSource.clip != null) {
            f_length = _thisAudioSource.clip.length;
        }
	}
	
	// Update is called once per frame
	void Update () {
        Ended();
	}

    private void Ended() {
        if (!b_ended)
        {
            f_length -= 1.0f * Time.deltaTime;
            if (f_length <= 0f)
            {
                f_length = 0.0f;
                b_ended = true;
            }
        }
    }

    public void Stop() {
        _thisAudioSource.Stop();
        f_length = 0f;
        b_ended = true;
    }

    public void Play(AudioClip clip, float timer = 0f)
    {
        _thisAudioSource.clip = clip;
        f_length = _thisAudioSource.clip.length;
        _thisAudioSource.PlayDelayed(timer);
    }
}
