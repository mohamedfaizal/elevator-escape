﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsControl : MonoBehaviour {
    public List<Sprite> _creditSprites;
    public float f_transitTime;

    private float f_timer;
    private int i_currentCredits;
    private bool b_ended;
    private bool b_restart;
    
    // Cache
    private GameObject _creditsCanvas;
    private Image _creditsHolder;
    private Fader _fader;
    private LevelHandler _levelHandler;
    private const float f_time = 1f;

    private void Awake()
    {
        f_timer = 0f;
        i_currentCredits = 0;
        b_ended = false;
        b_restart = false;
        // Cache
        _creditsCanvas = GameObject.Find("CreditsCanvas");
        _creditsCanvas.transform.GetChild(0).gameObject.SetActive(true);
        _creditsHolder = GameObject.FindGameObjectWithTag("Credits").GetComponent<Image>();
        _fader = GameObject.FindGameObjectWithTag("Fader").GetComponent<Fader>();
        _levelHandler = GameObject.Find("LevelHandler").GetComponent<LevelHandler>();
    }
    // Use this for initialization
    void Start () {
        AudioManager.GetInstance().AssignBackgroundMusic("TutorialBGM", true, true);
        _fader.ToFadeOut();
    }
	
	// Update is called once per frame
	void Update () {
        if (!b_ended)
        {
            f_timer += f_time * Time.deltaTime;
            if (f_timer > f_transitTime)
            {
                Ended();
            }
        }

        else {
            if (!b_restart)
            {
                if (_levelHandler.ControllerPressed())
                {
                    b_restart = true;
                    _fader.ToInstantFadeIn();
                    _creditsCanvas.transform.GetChild(0).gameObject.SetActive(false);
                    _levelHandler.Restart();
                    _creditsHolder.sprite = _creditSprites[0];
                    AudioManager.GetInstance().StopBackgroundMusic();
                    AudioManager.GetInstance().Restart();
                    ScriptFlowManager.GetInstance().Restart();
                }
            }
        }
	}

    private void Ended() {
        if (i_currentCredits + 1 < _creditSprites.Count)
        {
            ++i_currentCredits;
            _creditsHolder.sprite = _creditSprites[i_currentCredits];
            f_timer = 0f;

            if (i_currentCredits == _creditSprites.Count - 1)
            {
                b_ended = true;
            }
        }
    }
}
