﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Splashpage Controls
 Usage:
 Used for animations of the scene 'Splashpage'. Attach this to
 an empty GameObject for usage.

 Description:
 Animation for the scene 'Splashpage'.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/
public class SplashpageControls : MonoBehaviour {
    private Fader _fader;

    private float f_splashpageTimer;
    private const float f_splashpageTime = 3.0f;
    private const float f_startBGMTimer = 6.0f;
    private const float f_changeSceneTimer = 9.0f;

    private bool b_isLoaded;
    private bool b_playSound;
    private bool b_startBGM;

    private void Awake()
    {
        _fader = FindObjectOfType<Fader>();
        f_splashpageTimer = 0.0f;

        b_isLoaded = false;
        b_playSound = false;
        b_startBGM = false;
    }
    // Use this for initialization
    void Start()
    {
        StartCoroutine(StartScene());
    }

    /*****************************************************************
     Starts the scene once scene has been loaded.

     Return Type: IEnumerator
     Parameter: void
    *****************************************************************/
    private IEnumerator StartScene() {
        yield return new WaitForEndOfFrame();
        _fader.ToFadeOut();
        b_isLoaded = true;
    }

    // Update is called once per frame
    void Update() {
        if (_fader.Standby && b_isLoaded)
        {
            f_splashpageTimer += 1.0f * Time.deltaTime;
            if (f_splashpageTimer > f_splashpageTime)
            {
                _fader.ToInstantFadeIn();
                _fader.ActiveSplashPage(false);

                if (!b_playSound)
                {
                    b_playSound = true;
                    AudioManager.GetInstance().PlaySoundEffect("Boom", new Vector3(0, 0, 0));
                }

            }

            if (f_splashpageTimer > f_startBGMTimer) {
                if (!b_startBGM) {
                    b_startBGM = true;
                    AudioManager.GetInstance().AssignBackgroundMusicDelayed("TutorialBGM", false, true, 3);
                }
            }

            if (f_splashpageTimer > f_changeSceneTimer)
            {
                f_splashpageTimer = 0.0f;
                StartCoroutine(LevelManager.GetInstance().LoadLevel("Tutorial", true));
            }
        }
    }

    private void OnDestroy()
    {
        _fader = null;
    }
}
