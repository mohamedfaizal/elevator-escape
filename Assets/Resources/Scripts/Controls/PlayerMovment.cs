﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

/****************************************************************
 Player Movement
 Usage:
 Tracks player's movement. Attach this script to [Camera] GameObject.

 Description:
 Updates the player's position in the game based on real-life position.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/
public class PlayerMovment : MonoBehaviour {
    public GameObject _cameraEye;
    //public Fader _fader;
    //public float f_hitDistance = 0.5f;

    //private Vector3 _previousPos;
    //private Vector3 _gazeView;
    //private bool b_adjust;

    // Cache
    //private Camera m_Gaze;

    private void Awake()
    {
        //m_Gaze = _cameraEye.GetComponent<Camera>();
        //_gazeView = new Vector3(0.5f, 0.5f, 0f);
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        //Ray ray = m_Gaze.ViewportPointToRay(_gazeView);
        //RaycastHit hit;
        //if (Physics.Raycast(ray, out hit, f_hitDistance))
        //{
        //    InputTracking.disablePositionalTracking = true;
        //    _previousPos = new Vector3(transform.position.x, 0.0f, transform.position.z);
        //    b_adjust = true;
        //    _fader.ToFadeIn();
        //}

        //else if (!Physics.Raycast(ray, out hit, f_hitDistance)) {
        //    InputTracking.disablePositionalTracking = false;
        //    if (b_adjust) {
        //        _fader.ToFadeOut();
        //        transform.position = new Vector3(_previousPos.x, transform.position.y, _previousPos.z);

        //        if (_fader.Standby)
        //            b_adjust = false;
        //    }

        //}

        transform.position = new Vector3(_cameraEye.transform.localPosition.x, transform.position.y, _cameraEye.transform.localPosition.z);
    }
}
