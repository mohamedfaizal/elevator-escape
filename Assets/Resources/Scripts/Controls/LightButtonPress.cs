﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightButtonPress : MonoBehaviour {

    public GameObject liftLight;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(GetComponent<ButtonBehaviour>().b_buttonPressed)
        {
            if (liftLight.GetComponent<FlickerLights>().checkIfEventActive() == false)
            {
                AudioManager.GetInstance().PlaySoundEffect("Beep", new Vector3(0,0,0));
            }
            liftLight.GetComponent<FlickerLights>().triggerEventFlicker(false, true, -1);
            
        }
	}
}
