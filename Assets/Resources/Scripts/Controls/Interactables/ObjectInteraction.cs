﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Object Interaction:
 Used for HTC Vive's controllers. Attach this script to the
 controller's component.

 Affects how controllers interact with objects.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/

public class ObjectInteraction : MonoBehaviour {
    public InputBehaviour _controller;
    public BoxCollider _pressCollider;
    public BoxCollider _grabCollider;

    public GameObject _handPoint;
    public GameObject _handClosed;
    public GameObject _handOpened;

    private GameObject _collidingObject;
    public string CollidingObjectName {
        get {
            string _tempName = null;
            if (_collidingObject != null)
                _tempName =  _collidingObject.name;

            return _tempName;
        }
    }
    private GameObject _objectInHand;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!_controller.b_isGrabbing)
        {
            if (_controller.TouchPadPress())
            {
                _pressCollider.enabled = false;
                _grabCollider.enabled = true;

                _handPoint.SetActive(false);
                _handClosed.SetActive(false);
                _handOpened.SetActive(true);

                if (_collidingObject)
                {
                    GrabObject();
                    _controller.b_isGrabbing = true;

                    _handPoint.SetActive(false);
                    _handClosed.SetActive(true);
                    _handOpened.SetActive(false);
                }
            }

            else if (!_controller.TouchPadPress())
            {
                if (!_objectInHand)
                {
                    _controller.b_isGrabbing = false;
                    _pressCollider.enabled = true;
                    _grabCollider.enabled = false;

                    _handPoint.SetActive(true);
                    _handClosed.SetActive(false);
                    _handOpened.SetActive(false);
                }
            }
        }

        if (_controller.b_isGrabbing)
        {
            if (_controller.TouchPadUp())
            {
                if (_objectInHand)
                {
                    ReleaseObject();
                    _controller.b_isGrabbing = false;
                    _pressCollider.enabled = true;
                    _grabCollider.enabled = false;

                    _handPoint.SetActive(true);
                    _handClosed.SetActive(false);
                    _handOpened.SetActive(false);
                }
            }
        }
    }

    /*****************************************************************
     Adds a fixed joint to the grabbed object.

     Return Type: FixedJoint
    *****************************************************************/
    private FixedJoint AddFixedJoint()
    {
        FixedJoint _fixedJoint = gameObject.AddComponent<FixedJoint>();
        _fixedJoint.breakForce = 20000;
        _fixedJoint.breakTorque = 20000;

        return _fixedJoint;
    }

    /*****************************************************************
     Grabs the interactable object.

     Return Type: void
    *****************************************************************/
    private void GrabObject()
    {
        if (!_objectInHand)
        {
            _objectInHand = _collidingObject;
            _collidingObject = null;

            FixedJoint joint = AddFixedJoint();
            joint.connectedBody = _objectInHand.GetComponent<Rigidbody>();
        }
    }

    /*****************************************************************
     Releases the interactable object.

     Return Type: void
    *****************************************************************/
    private void ReleaseObject()
    {
        if (GetComponent<FixedJoint>())
        {
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());

            _objectInHand.GetComponent<Rigidbody>().velocity = _controller.Controller.velocity;
            _objectInHand.GetComponent<Rigidbody>().angularVelocity = _controller.Controller.angularVelocity;
        }

        _objectInHand = null;
    }

    /*****************************************************************
     Checks if the controller is colliding with other interactable
     objects.

     Parameter: Collider
     Return Type: void
    *****************************************************************/
    private void SetCollidingObject(Collider collider)
    {
        if (_collidingObject || collider.gameObject.tag != "Interactables")
            return;

        _collidingObject = collider.gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        SetCollidingObject(other);
    }

    private void OnTriggerStay(Collider other)
    {
        SetCollidingObject(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!_collidingObject)
            return;

        _collidingObject = null;
    }
}
