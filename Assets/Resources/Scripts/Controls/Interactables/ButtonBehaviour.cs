﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/****************************************************************
 Button Behaviour
 Usage:
 Used for any objects that acts as a button. Attach this script
 to the button GameObject.

 Description:
 Detects if it is a "One-time press" event trigger, or it is an
 immediate trigger.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/
public class ButtonBehaviour : MonoBehaviour {
    public Material _pressedMat;
    public Material _unpressedMat;
    public bool b_isEventTrigger;
    public SoundEffectHandler _thisSoundEffect;

    public bool b_buttonPressed;
    private bool b_buttonInteracted;

    public bool ButtonInteracted {
        get { return b_buttonInteracted; }
    }

    private void Awake()
    {
        b_buttonPressed = false;
        b_buttonInteracted = false;

    }
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (b_buttonInteracted) {
            if (!b_buttonPressed) {
                b_buttonPressed = true;
                GetComponent<Renderer>().material = _pressedMat;
            }         
        }
	}

    /*****************************************************************
     Resets any necessary variables for the script

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void ResetButton() {
        b_buttonPressed = false;
        GetComponent<Renderer>().material = _unpressedMat;
    }

    /*****************************************************************
     Checks if this particular button has been interacted with any
     means.

     Return Type: bool
     Parameter: void
    *****************************************************************/
    public bool IsButtonInteracted() {
        return b_buttonInteracted;
    }

    /*****************************************************************
     Checks if this particular button is an "One-time" press event
     trigger button.

     Return Type: bool
     Parameter: void
    *****************************************************************/
    public bool IsEventTriggerButton() {
        return b_isEventTrigger;
    }

    /*****************************************************************
     Checks if this particular button has been interacted with any
     controllers.

     Return Type: void
     Parameter: void
    *****************************************************************/
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "LeftController" || collision.gameObject.tag == "RightController") {
            b_buttonInteracted = true;
            _thisSoundEffect.Play(true, false);
        }
    }

    /*****************************************************************
     Checks if this particular button has been interacted with any
     controllers.

     Return Type: void
     Parameter: void
    *****************************************************************/
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "LeftController" || collision.gameObject.tag == "RightController") {
            b_buttonInteracted = true;
        }
    }

    /*****************************************************************
     Checks if this particular button's collision has been exited.

     Return Type: void
     Parameter: void
    *****************************************************************/
    private void OnCollisionExit(Collision collision)
    {
        b_buttonInteracted = false;
        if (b_isEventTrigger) {
            b_buttonPressed = false;
            GetComponent<Renderer>().material = _unpressedMat;
        }

        _thisSoundEffect.Play(true, false);
    }
}
