﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Object Fader Script:
 Changes the opacity of the object this is tied to and it's children objects

 Noel Ho Sing Nam
 noelhosingnam@hotmail.com/ noelhosingnam@viziofly.com
 +65-86111251
 ****************************************************************/

public class ObjectFader : MonoBehaviour {

    enum FADE_STATE
    {
        FADE_STANDBY,
        FADE_FADEOUT,
        FADE_FADEIN
    }

    private FADE_STATE _currentFadeState;
    private Component[] renderers;

    [Tooltip("How fast an object fades, if not based on timing")]
    public float f_fadeRate = 0.01f;
    [Tooltip("Maximum Opacity")]
    public float f_fadeInLimit = 1.00f;
    [Tooltip("Minimum Opacity")]
    public float f_fadeOutLimit = 0.00f;
    [Tooltip("Current / Starting Opacity")]
    public float f_objectOpacity = 1.0f;

    private Color currentColor;
    private float f_timer;

    /*****************************************************************
     Use this for initialization

     Return Type: void
     Parameter: void
    *****************************************************************/
    void Start () {
        renderers = GetComponentsInChildren<Renderer>();
        _currentFadeState = FADE_STATE.FADE_STANDBY;
        currentColor = new Color(1, 1, 1);
        SetOpacity(f_objectOpacity);

        foreach (Renderer texture in renderers)
        {
            currentColor = texture.material.color;
            texture.material.SetFloat("_Mode", 2);
            texture.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, f_objectOpacity);
            texture.material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            texture.material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            texture.material.DisableKeyword("_ALPHATEST_ON");
            texture.material.EnableKeyword("_ALPHABLEND_ON");
            texture.material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
            texture.material.renderQueue = 3000;
        }
    }

    /*****************************************************************
     Update is called once per frame

     Return Type: void
     Parameter: void
    *****************************************************************/
    void Update() {
        if (Input.GetKeyDown(KeyCode.H))
        {
            ToFadeIn(5.0f);
        }
        if(Input.GetKeyDown(KeyCode.J))
        {
            ToFadeOut(1.0f);
        }
        switch(_currentFadeState)
        {
            case FADE_STATE.FADE_STANDBY:
                {

                }
                break;

            case FADE_STATE.FADE_FADEOUT:
                {
                    FadeOut();
                }
                break;

            case FADE_STATE.FADE_FADEIN:
                {
                    FadeIn();
                }
                break;
        }
    }

    /*****************************************************************
     Changes the current opacity

     Return Type: void
     Parameter: float
    *****************************************************************/
    public void SetOpacity(float alpha)
    {
        if (alpha >= 0.0f && alpha <= 1.0f)
        {
            f_objectOpacity = alpha;
            foreach (Renderer texture in renderers)
            {
                currentColor = texture.material.color;
                texture.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, f_objectOpacity);
            }
        }
    }

    /*****************************************************************
     Changes the Maximum opacity

     Return Type: void
     Parameter: float
    *****************************************************************/
    public void SetFadeInLimit(float newLimit)
    {
        if(newLimit >= 0.0f && newLimit <= 1.0f)
        {
            f_fadeInLimit = newLimit;
        }
    }

    /*****************************************************************
     Changes the Minimum opacity

     Return Type: void
     Parameter: float
    *****************************************************************/
    public void SetFadeOutLimit(float newLimit)
    {
        if (newLimit >= 0.0f && newLimit <= 1.0f)
        {
            f_fadeOutLimit = newLimit;
        }
    }

    /*****************************************************************
     Checks if it is not in the middle of fading

     Return Type: bool
     Parameter: void
    *****************************************************************/
    public bool IsFadeDone()
    {
        return (_currentFadeState == FADE_STATE.FADE_STANDBY);
    }

    /*****************************************************************
     Fades to opaque

     Return Type: void
     Parameter: float
    *****************************************************************/
    public void ToFadeIn(float time = -1.0f)
    {
        if (time >= 0.0f)
        {
            if (time == 0.0f)
            {
                SetOpacity(f_fadeInLimit);
            }
            else
            {
                float fadeDifference = f_fadeInLimit - f_objectOpacity;
                f_fadeRate = fadeDifference / (time / Time.deltaTime);
            }
        }

        _currentFadeState = FADE_STATE.FADE_FADEIN;
    }

    /*****************************************************************
     Fades to transparent

     Return Type: void
     Parameter: float
    *****************************************************************/
    public void ToFadeOut(float time = -1.0f)
    {
        if (time >= 0.0f)
        {
            if (time == 0.0f)
            {
                SetOpacity(f_fadeOutLimit);
            }
            else
            {
                float fadeDifference = f_objectOpacity - f_fadeOutLimit;
                f_fadeRate = fadeDifference / (time / Time.deltaTime);
            }
        }

        _currentFadeState = FADE_STATE.FADE_FADEOUT;
    }

    /*****************************************************************
     private Fades to transparent

     Return Type: void
     Parameter: void
    *****************************************************************/
    private void FadeOut()
    {
        bool b_doneFading = true;
        foreach (Renderer texture in renderers)
        {
            if (texture.material.color.a > f_fadeOutLimit)
            {
                b_doneFading = false;
                currentColor = texture.material.color;
                texture.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, currentColor.a - f_fadeRate);
            }
        }

        f_objectOpacity -= f_fadeRate;
        if(b_doneFading == true)
        {
            _currentFadeState = FADE_STATE.FADE_STANDBY;
        }
    }

    /*****************************************************************
     private Fades to opaque

     Return Type: void
     Parameter: float
    *****************************************************************/
    private void FadeIn()
    {
        bool b_doneFading = true;
        foreach (Renderer texture in renderers)
        {
            if (texture.material.color.a < f_fadeInLimit)
            {
                b_doneFading = false;
                currentColor = texture.material.color;
                texture.material.color = new Color(currentColor.r, currentColor.g, currentColor.b, currentColor.a + f_fadeRate);
            }
        }

        f_objectOpacity += f_fadeRate;
        if (b_doneFading == true)
        {
            _currentFadeState = FADE_STATE.FADE_STANDBY;
        }
    }
}
