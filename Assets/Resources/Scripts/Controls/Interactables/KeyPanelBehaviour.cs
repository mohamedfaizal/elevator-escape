﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/****************************************************************
 Key Panel Behaviour:
 Manages button and password for Corridor scene

 Noel Ho Sing Nam
 noelhosingnam@hotmail.com/ noelhosingnam@viziofly.com
 +65-86111251
 ****************************************************************/

public class KeyPanelBehaviour : MonoBehaviour {
    public List<ButtonBehaviour> _buttonList;
    public SoundEffectHandler _SFXHandler;
    public int i_maxPasswordCount = 3;
    public string str_passwordCombination;
    public Text _password;

    private List<int> _inputList;
    private List<bool> _buttonPressedList;
    private int i_pressedCount;
    private string str_input;
    private bool b_matchFound;

    public bool MatchIsFound {
        get { return b_matchFound; }
    }

    // Cache
    private int i_totalButtonCount;

    private void Awake()
    {
        _inputList = new List<int>();
        _buttonPressedList = new List<bool>();
        i_totalButtonCount = _buttonList.Count;
        i_pressedCount = 0;
        str_input = null;
        b_matchFound = false;
    }

    // Use this for initialization
    void Start () {
        Initialize();
    }

    private void Initialize() {
        string tempString = null;
        int i_totalButtons = _buttonList.Count;
        if (i_totalButtons <= 0) {
            return;
        }

        if (i_maxPasswordCount > i_totalButtons) {
            i_maxPasswordCount = i_totalButtons;
        }

        int randomed;
        for (int i = 0; i < i_totalButtons; ++i) {
            bool check = false;

            do {
                randomed = Random.Range(1, i_totalButtons + 1);
                check = true;

                for (int j = 0; j < i; ++j) {
                    if (randomed == _inputList[j]) {
                        check = false;
                        break;
                    }
                }
            } while (!check);

            _inputList.Add(randomed);
            _buttonPressedList.Add(false);
        }

        for (int i = 0; i < i_maxPasswordCount; ++i) {
            tempString = tempString + str_passwordCombination[i];
        }

        str_passwordCombination = tempString;
    }
	
	// Update is called once per frame
	void Update () {
        KeyCombinationPuzzleChecker();
    }

    private void KeyCombinationPuzzleChecker() {
        if (!b_matchFound) {
            if (i_pressedCount < i_totalButtonCount - 1) {
                for (int i = 0; i < i_totalButtonCount; ++i) {
                    if (_buttonList[i].b_buttonPressed && !_buttonPressedList[i]) {
                        _buttonPressedList[i] = true;
                        str_input = str_input + _inputList[i].ToString();
                        ++i_pressedCount;
                    }
                }
            }

            else {
                for (int i = 0; i < i_totalButtonCount; ++i) {
                    _buttonList[i].ResetButton();
                    _buttonPressedList[i] = false;
                    str_input = "";
                    i_pressedCount = 0;
                }
            }

            if (str_input == str_passwordCombination) {
                b_matchFound = true;
            }
        }

        _password.text = str_input;
    }

    public void OpenServicePanel() {
        _SFXHandler.Play();
        GetComponent<Rigidbody>().AddForce(-Vector3.forward * 10.0f, ForceMode.Impulse);
    }
}
