﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Pregame Controls
 Usage:
 Used to load the title of the game, and loads any objects/ scripts
 if necessary. Attach this script to an empty GameObject.

 Description:
 Used to load the title of the game, and loads any objects/ scripts
 if necessary.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/
public class PregameControls : MonoBehaviour {
    private GameObject _pregameCanvas;
    private Fader _fader;

    private bool b_isLoaded;
    private float f_timer;
    private bool b_ready;
    private const float f_fadeInTime = 7f;
    private const float f_seconds = 1.0f;

    void Awake()
    {
        _pregameCanvas = GameObject.Find("PregameCanvas");
        _fader = GameObject.FindGameObjectWithTag("Fader").GetComponent<Fader>();

        _pregameCanvas.transform.GetChild(0).gameObject.SetActive(true);

        b_isLoaded = false;
    }

	// Use this for initialization
	void Start () {
        StartCoroutine(StartScene());
	}

    /*****************************************************************
     Starts the scene once everything is loaded.

     Return Type: IEnumerator
     Parameter: void
    *****************************************************************/
    private IEnumerator StartScene()
    {
        yield return new WaitForSeconds(2.0f);
        _fader.ToFadeOut();
        b_isLoaded = true;

        AudioManager.GetInstance().AssignBackgroundMusic("PregameOpening", true, true);
    }

    // Update is called once per frame
    void Update () {
        if (b_isLoaded) {
            if (!b_ready) {
                f_timer += f_seconds * Time.deltaTime;
                if (f_timer > f_fadeInTime) {
                    _fader.ToInstantFadeIn();
                    b_ready = true;
                }
            }

            if (AudioManager.GetInstance().IsCurrentBGMFinished()) {
                ToLoadGame();
            }
        }
	}

    /*****************************************************************
     Proceeds to start the game level.

     Return Type: void
     Parameter: bool
    *****************************************************************/
    private void ToLoadGame() {
        if (_fader.Standby) {
            b_isLoaded = false;
            StartCoroutine(LevelManager.GetInstance().LoadLevel("Elevator", true));
        }
    }

    private void OnDestroy()
    {
        _pregameCanvas.transform.GetChild(0).gameObject.SetActive(false);
        _pregameCanvas = null;
        _fader = null;
    }
}
