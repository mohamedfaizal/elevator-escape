﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsEffect : MonoBehaviour {
    [Range(0f, 1.5f)]
    [SerializeField]
    [Tooltip("Affects the intensity of the light.")]
    private float f_intensity;

    public float Intensity {
        get { return f_intensity; }
        set { f_intensity = value; }
    }

    [Range(0f, 1.5f)]
    [SerializeField]
    [Tooltip("The maximum intensity of the light.")]
    private float f_maxIntensity;
    private const float f_minIntensity = 0f;

    public float MaxIntensity {
        get { return f_maxIntensity; }
        set { f_maxIntensity = value; }
    }

    [Range(0.1f, 5f)]
    [SerializeField]
    [Tooltip("The rate of change of the intensity.")]
    private float f_speedRate;

    public float SpeedRate {
        get { return f_speedRate; }
        set { f_speedRate = value; }
    }

    [Range(0.1f, 2f)]
    [SerializeField]
    [Tooltip("The rate of the next light animation.")]
    private float f_interval;

    public float Interval {
        get { return f_interval; }
        set { f_interval = value; }
    }

    [Range(0.1f, 0.5f)]
    [SerializeField]
    [Tooltip("The rate of the static current to animate.")]
    private float f_staticInterval;

    public float StaticInterval {
        get { return f_staticInterval; }
        set { f_interval = value; }
    }

    private float f_staticIntervalTimer;

    [Range(0.1f, 3f)]
    [SerializeField]
    [Tooltip("The time taken for the next static to happen.")]
    private float f_staticDuration;

    public float StaticDuration {
        get { return f_staticDuration; }
        set { f_staticDuration = value; }
    }

    private float f_staticDurationTimer;
    private bool b_onStatic;

    [Range(1f, 10f)]
    [SerializeField]
    [Tooltip("Sets the duration of 1 effect.")]
    private float f_maxDuration;

    public float Duration {
        get { return f_maxDuration; }
        set { f_maxDuration = value; }
    }

    [SerializeField]
    [Tooltip("If the light is on or off. Used for initialization")]
    private bool b_lightOn;

    public bool ToggleSwitch {
        get { return b_lightOn; }
    }

    private bool b_inAnimation;
    public bool StartAnimation {
        get { return b_inAnimation; }
        set { b_inAnimation = true; }
    }

    public bool StopAnimation {
        set { b_inAnimation = false; }
    }

    private bool b_endedAnimation;
    public bool AnimationEnded {
        get { return b_endedAnimation; }
    }

    private float f_intervalTimer;
    private float f_duration;

    // Cache
    public Light _light;
    private const float f_time = 1.0f;

    private bool b_initialSwitchOn;
    private bool b_finalSwitchOn;

    private void Awake()
    {
        f_intervalTimer = 0f;
        f_duration = 0f;

        if (!b_lightOn) {
            _light.intensity = 0f;
        }

        if (f_intensity > f_maxIntensity)
            f_intensity = f_maxIntensity;

        _light.intensity = f_intensity;
        f_intervalTimer = 0f;

        b_initialSwitchOn = b_lightOn;
        b_finalSwitchOn = false;
        b_onStatic = false;
        f_staticIntervalTimer = 0f;
        f_staticDurationTimer = 0f;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void FlickerEffect(bool finalSwitchOn, bool customize = false, float interval = 0.1f) {
        b_finalSwitchOn = finalSwitchOn;
        if (b_inAnimation) {
            f_intervalTimer += f_time * Time.deltaTime;
            f_duration += f_time * Time.deltaTime;
            if (f_duration < f_maxDuration) {
                if (!customize) {
                    if (f_intervalTimer > f_interval) {
                        if (!b_initialSwitchOn) {
                            b_initialSwitchOn = true;
                            _light.intensity = f_minIntensity;
                        }

                        else {
                            b_initialSwitchOn = false;                      
                            _light.intensity = f_intensity;
                        }
                        f_intervalTimer = 0f;
                    }
                }

                else {
                    if (f_intervalTimer > interval) {
                        if (!b_initialSwitchOn)
                        {
                            b_initialSwitchOn = true;
                            _light.intensity = f_minIntensity;
                        }

                        else {
                            _light.intensity = f_intensity;
                        }
                        f_intervalTimer = 0f;
                    }
                }
            }

            else {
                if (b_finalSwitchOn) {
                    _light.intensity = f_intensity;
                }

                else {
                    _light.intensity = f_minIntensity;
                }

                f_intervalTimer = 0f;
                f_duration = 0f;
                b_inAnimation = false;
                b_endedAnimation = true;
            }
        }

    }

    public void ContinueTimedFlickerEffect() {
        if (b_inAnimation) {
            f_intervalTimer += f_time * Time.deltaTime;
            if (f_intervalTimer > f_interval) {
                f_intervalTimer = 0f;
                if (!b_initialSwitchOn)
                {
                    b_initialSwitchOn = true;
                    _light.intensity = f_intensity;
                }

                else
                {
                    b_initialSwitchOn = false;
                    _light.intensity = f_minIntensity;
                }
            }
        }
    }

    public void StaticFlickerEffect() {
        if (b_inAnimation) {
            if (!b_onStatic) {
                f_staticDurationTimer += f_time * Time.deltaTime;
                if (f_staticDurationTimer > f_staticDuration) {
                    f_staticDurationTimer = 0f;
                    b_onStatic = true;
                    if (b_initialSwitchOn)
                    {
                        b_initialSwitchOn = false;
                        _light.intensity = f_minIntensity;
                        AudioManager.GetInstance().PlaySoundEffect("Light Burnt", _light.gameObject.transform.position);
                    }

                    else {
                        b_initialSwitchOn = true;
                        _light.intensity = f_intensity;
                    }                    
                }
            }
            else {
                f_staticIntervalTimer += f_time * Time.deltaTime;
                if (f_staticIntervalTimer > f_staticInterval) {
                    if (b_initialSwitchOn) {
                        b_initialSwitchOn = false;
                        _light.intensity = f_minIntensity;
                        AudioManager.GetInstance().PlaySoundEffect("Light Burnt", _light.gameObject.transform.position);
                    }

                    else {
                        b_initialSwitchOn = true;
                        _light.intensity = f_intensity;
                    }

                    f_staticIntervalTimer = 0f;
                    b_onStatic = false;                  
                }
            }
        }
    }

    public void SwitchOffLight() {
        _light.intensity = f_minIntensity;
    }

    public void GradualTimedSwitchOffLight() {
        if (b_inAnimation)
        {
            _light.intensity -= f_time * Time.deltaTime;
            if (_light.intensity <= f_minIntensity)
            {
                _light.intensity = f_minIntensity;
                b_inAnimation = false;
                b_endedAnimation = true;
            }
        }
    }

    public void SwitchOnLight() {
        _light.intensity = f_intensity;
    }

    public void SwitchOnLightMax() {
        _light.intensity = f_maxIntensity;
    }

    public void GradualTimedSwitchOnLight() {
        if (b_inAnimation) {
            _light.intensity += f_time * Time.deltaTime;
            if (_light.intensity > f_intensity) {
                _light.intensity = f_intensity;
                b_inAnimation = false;
                b_endedAnimation = true;
            }
        }
    }

    public void GradualTimedSwitchOnLightMax() {
        if (b_inAnimation) {
            _light.intensity += f_time * Time.deltaTime;
            if (_light.intensity > f_maxIntensity) {
                _light.intensity = f_maxIntensity;
                b_inAnimation = false;
                b_endedAnimation = true;
            }
        }
    }

    public void ResetAnimationStatus() {
        f_staticIntervalTimer = 0f;
        f_staticDurationTimer = 0f;
        b_inAnimation = false;
        b_endedAnimation = false;
    }
}
