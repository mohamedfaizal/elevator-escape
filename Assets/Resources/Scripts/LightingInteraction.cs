﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LIGHT_ANIMATION
{
    LIGHT_ANIMATION_START,
    LIGHT_ANIMATION_FLICKER,
    LIGHT_ANIMATION_FLICKER_DELAYED,
    LIGHT_ANIMATION_FLICKER_CONTINUED,
    LIGHT_ANIMATION_FLICKER_STATIC,
    LIGHT_ANIMATION_OFF,
    LIGHT_ANIMATION_OFF_GRADUAL,
    LIGHT_ANIMATION_ON,
    LIGHT_ANIMATION_ON_MAX,
    LIGHT_ANIMATION_ON_GRADUAL,
    LIGHT_ANIMATION_ON_GRADUAL_MAX,
    LIGHT_ANIMATION_END
}

public class LightingInteraction : MonoBehaviour {
    [Tooltip("Attach the light script with the light\n" +
        "source. LightsEffect script must be in the inspector for" +
        "the light to be able to be attached.")]
    public List<LightsEffect> _lights;

    [Range(0f, 2f)]
    [SerializeField]
    [Tooltip("The max time for delay.")]
    private float f_maxDelayTime;
    public float MaxDelayTime
    {
        get { return f_maxDelayTime; }
    }

    private float f_delay = 0f;
    public float DelayTime {
        get { return f_delay; }
        set
        {
            if (value >= f_maxDelayTime)
                f_delay = f_maxDelayTime;

            else
                f_delay = value;
        }
    }

    private LIGHT_ANIMATION _LIGHT_ANIMATION;
    public bool AnimationEnd {
        get { return _LIGHT_ANIMATION == LIGHT_ANIMATION.LIGHT_ANIMATION_END; }
    }

    // Cache
    private float f_highestDuration;
    private int i_highestDurationLightSlot;
    private int i_totalLightAmount;
    private const float f_time = 1f;
    private int i_currentCounter = 0;
    private bool b_startedAll;

    private void Awake()
    {
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_START;
        f_highestDuration = 0f;
        i_totalLightAmount = _lights.Count;
        b_startedAll = false;
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        switch (_LIGHT_ANIMATION) {
            case LIGHT_ANIMATION.LIGHT_ANIMATION_START:
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_FLICKER:
                FlickeringLights();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_FLICKER_DELAYED:
                FlickeringLightsDelayed();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_FLICKER_CONTINUED:
                FlickeringLightsContinued();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_OFF:
                SwitchOff();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_OFF_GRADUAL:
                GradualSwitchOff();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_FLICKER_STATIC:
                FlickeringStaticLights();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_ON:
                SwitchOn();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_ON_MAX:
                SwitchOnMax();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_ON_GRADUAL:
                GradualSwitchOn();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_ON_GRADUAL_MAX:
                GradualSwitchOnMax();
                break;

            case LIGHT_ANIMATION.LIGHT_ANIMATION_END:
                break;

            default:
                break;
        }
	}

    public void ToAnimationFlicker() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            if (!_lights[i].StartAnimation)
                _lights[i].StartAnimation = true;
            if (f_highestDuration <= _lights[i].Duration)
            {
                f_highestDuration = _lights[i].Duration;
                i_highestDurationLightSlot = i;
            }
        }

        b_startedAll = true;
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_FLICKER;
    }

    public void ToAnimationFlickerDelayed() {
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_FLICKER_DELAYED;
    }

    public void ToAnimationFlickerContinued() {
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_FLICKER_CONTINUED;
    }

    public void ToAnimationStatic() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].StartAnimation = true;
        }

        b_startedAll = true;
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_FLICKER_STATIC;
    }

    public void ToAnimationLightOff() {
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_OFF;
    }

    public void ToAnimationGradualLightOff() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].StartAnimation = true;
        }
        b_startedAll = true;
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_OFF_GRADUAL;
    }

    public void ToAnimationLightOn() {
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_ON;
    }

    public void ToAnimationLightOnMax() {
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_ON_MAX;
    }

    public void ToAnimationGradualLightOn() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].StartAnimation = true;
        }
        b_startedAll = true;
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_ON_GRADUAL;
    }

    public void ToAnimationGradualLightOnMax() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].StartAnimation = true;
        }
        b_startedAll = true;
        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_ON_GRADUAL_MAX;
    }

    public void Restart()
    {
        if (_LIGHT_ANIMATION == LIGHT_ANIMATION.LIGHT_ANIMATION_END) {
            for (int i = 0; i < i_totalLightAmount; ++i)
            {
                _lights[i].ResetAnimationStatus();
            }

            _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_START;
        }
    }

    private void FlickeringLights() {
        for (int i = 0; i < i_totalLightAmount; ++i)
        {
            _lights[i].FlickerEffect(false);
        }

        if (b_startedAll)
        {
            if (_lights[i_highestDurationLightSlot].AnimationEnded)
            {
                b_startedAll = false;
                i_currentCounter = 0;
                _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_END;
            }
        }
    }

    private void FlickeringLightsDelayed() {
        if (!b_startedAll) {
            f_delay += f_time * Time.deltaTime;
            if (f_delay > f_maxDelayTime)
            {
                _lights[i_currentCounter].StartAnimation = true;
                if (f_highestDuration <= _lights[i_currentCounter].Duration)
                {
                    f_highestDuration = _lights[i_currentCounter].Duration;
                    i_highestDurationLightSlot = i_currentCounter;
                }
                if (i_currentCounter + 1 < i_totalLightAmount)
                    ++i_currentCounter;

                else
                {
                    b_startedAll = true;
                }
                f_delay = 0f;
            }
        }

        else {
            if (_lights[i_highestDurationLightSlot].AnimationEnded) {
                for (int i = 0; i < i_totalLightAmount; ++i) {
                    _lights[i].ResetAnimationStatus();
                }

                i_currentCounter = 0;
                _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_END;
            }
        }

        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].FlickerEffect(false);
        }
}

    private void FlickeringLightsContinued() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            if (!_lights[i].StartAnimation) {
                _lights[i].StartAnimation = true;
            }
            _lights[i].ContinueTimedFlickerEffect();
        }
    }

    private void FlickeringStaticLights() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].StaticFlickerEffect();
        }
    }

    private void SwitchOff() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].SwitchOffLight();
        }

        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_END;
    }

    private void GradualSwitchOff() {
        bool checker = false;
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].GradualTimedSwitchOffLight();
        }

        for (int i = 0; i < i_totalLightAmount; ++i) {
            if (_lights[i].AnimationEnded) {
                checker = true;
            }

            else {
                checker = false;
                break;
            }
        }

        if (checker) {
            _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_END;
        }
    }

    private void SwitchOn() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].SwitchOnLight();
        }

        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_END;
    }

    private void SwitchOnMax() {
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].SwitchOnLightMax();
        }

        _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_END;
    }

    private void GradualSwitchOn() {
        bool checker = false;
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].GradualTimedSwitchOnLight();
        }

        for (int i = 0; i < i_totalLightAmount; ++i) {
            if (_lights[i].AnimationEnded) {
                checker = true;
            }

            else {
                checker = false;
                break;
            }
        }

        if (checker) {
            _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_END;
        }
    }

    private void GradualSwitchOnMax() {
        bool checker = false;
        for (int i = 0; i < i_totalLightAmount; ++i) {
            _lights[i].GradualTimedSwitchOnLightMax();
        }

        for (int i = 0; i < i_totalLightAmount; ++i) {
            if (_lights[i].AnimationEnded)
            {
                checker = true;
            }

            else {
                checker = false;
                break;
            }
        }

        if (checker) {
            _LIGHT_ANIMATION = LIGHT_ANIMATION.LIGHT_ANIMATION_END;
        }
    }
}
