﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Script Flow Manager
 Usage:
 A manager for all of the scripts in the game. Calls its Instance
 for usage.

 Description:
 A manager for all of the scripts in the game.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/

public class ScriptFlowManager {
    private static ScriptFlowManager _scriptFlowManager = null;

    public enum SCRIPT_FLOW_STATE {
        SCRIPT_FLOW_START,
        SCRIPT_FLOW_INTRODUCTION,
        SCRIPT_FLOW_CORRIDOR,
        SCRIPT_FLOW_LAST,
        SCRIPT_FLOW_END
    }

    private SCRIPT_FLOW_STATE _SCRIPT_FLOW_STATE;

    public enum SCRIPT_INTRODUCTION {
        SCRIPT_INTRODUCTION_START,
        SCRIPT_INTRODUCTION_INTERCOM_1,
        SCRIPT_INTRODUCTION_SOUNDEFFECT_1,
        SCRIPT_INTRODUCTION_UNKNOWN_1,
        SCRIPT_INTRODUCTION_SOUNDEFFECT_2,
        SCRIPT_INTRODUCTION_INTERCOM_2,
        SCRIPT_INTRODUCTION_END,
        SCRIPT_INTRODUCTION_DISABLE
    }

    public enum SCRIPT_LAST {
        SCRIPT_LAST_START,
        SCRIPT_LAST_INTERCOM_1,
        SCRIPT_LAST_ELEVATOR_STALL_SFX_1,
        SCRIPT_LAST_INTERCOM_2,
        SCRIPT_LAST_UNKNOWN_1,
        SCRIPT_LAST_SOUNDEFFECT_1,
        SCRIPT_LAST_INTERCOM_3,
        SCRIPT_LAST_SOUNDEFFECT_2,
        SCRIPT_LAST_END,
        SCRIPT_LAST_DISABLE
    }

    private SCRIPT_INTRODUCTION _SCRIPT_INTRODUCTION;
    private SCRIPT_LAST _SCRIPT_LAST;

    /*****************************************************************
     Get the instance of ScriptFlowManager script

     Return Type: ScriptFlowManager
     Parameter: void
    *****************************************************************/
    public static ScriptFlowManager GetInstance() {
        if (_scriptFlowManager == null) {
            _scriptFlowManager = new ScriptFlowManager();
        }

        return _scriptFlowManager;
    }

    /*****************************************************************
     Initializes this script

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void Initialize() {
        _SCRIPT_FLOW_STATE = SCRIPT_FLOW_STATE.SCRIPT_FLOW_START;
        _SCRIPT_INTRODUCTION = SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_START;
        _SCRIPT_LAST = SCRIPT_LAST.SCRIPT_LAST_START;
    }

    /*****************************************************************
     Gets the current overall script flow

     Return Type: enum SCRIPT_FLOW_STATE
     Parameter: void
    *****************************************************************/
    public SCRIPT_FLOW_STATE GetScriptFlowState() {
        return _SCRIPT_FLOW_STATE;
    }

    public void NextScriptFlow() {
        if (_SCRIPT_FLOW_STATE + 1 != SCRIPT_FLOW_STATE.SCRIPT_FLOW_END) {
            ++_SCRIPT_FLOW_STATE;
        }

        else {
            _SCRIPT_FLOW_STATE = SCRIPT_FLOW_STATE.SCRIPT_FLOW_END;
        }
    }

    /*****************************************************************
     Gets the state of the introduction scriptflow

     Return Type: void
     Parameter: void
    *****************************************************************/
    public SCRIPT_INTRODUCTION GetScriptIntroductionState() {
        return _SCRIPT_INTRODUCTION;
    }

    public SCRIPT_LAST GetScriptLastState() {
        return _SCRIPT_LAST;
    }

    /*****************************************************************
     Proceeds to the next state of the introduction script

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void NextScriptIntroductionState() {
        if ((int)_SCRIPT_INTRODUCTION + 1 != (int)SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_DISABLE) {
            _SCRIPT_INTRODUCTION++;
        }

        else {
            _SCRIPT_INTRODUCTION = SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_DISABLE;
        }
    }

    public void NextScriptLastState() {
        if ((int)_SCRIPT_LAST + 1 != (int)SCRIPT_LAST.SCRIPT_LAST_DISABLE)
        {
            _SCRIPT_LAST++;
        }

        else {
            _SCRIPT_LAST = SCRIPT_LAST.SCRIPT_LAST_DISABLE;
        }
    }

    public bool ScriptIntroductionDisable() {
        return _SCRIPT_INTRODUCTION == SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_DISABLE;
    }

    public void Restart() {
        _SCRIPT_FLOW_STATE = SCRIPT_FLOW_STATE.SCRIPT_FLOW_START;
        _SCRIPT_INTRODUCTION = SCRIPT_INTRODUCTION.SCRIPT_INTRODUCTION_START;
        _SCRIPT_LAST = SCRIPT_LAST.SCRIPT_LAST_START;
    }
}
