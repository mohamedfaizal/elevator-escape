﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
/****************************************************************
 Audio Manager
 Usage:
 Calls the instance of the AudioManager

 Description:
 Handles all audio related items, and the AudioClips.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/

public class AudioManager {
    private static AudioManager _audioManager;

    private List<AudioClip> _BGMAudioList = new List<AudioClip>();
    private List<AudioClip> _SFXAudioList = new List<AudioClip>();
    private List<AudioClip> _VoiceAudioList = new List<AudioClip>();

    private AudioSource _voicePlayer;
    private AudioSource _BGMPlayer;

    private int i_currentVoiceSlot = 0;

    /*****************************************************************
     Calls the instance of AudioManager

     Return Type: AudioManager
     Parameter: void
    *****************************************************************/
    public static AudioManager GetInstance() {
        if (_audioManager == null) {
            _audioManager = new AudioManager();
        }

        return _audioManager;
    }

    /*****************************************************************
     Initializes AudioManager

     Return Type: void
     Parameter: AudioSource, AudioSource
    *****************************************************************/
    public void Initialize(AudioSource voice, AudioSource BGM) {
        ReadAndLoadAudioFiles();

        _voicePlayer = voice;
        _BGMPlayer = BGM;
    }

    /*****************************************************************
     Reads the text file and loads the audio clips.

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void ReadAndLoadAudioFiles() {
        string path = "Assets/Resources/Sounds/SoundEffects/soundEffectList.txt";
        StreamReader reader = new StreamReader(path);
        while (!reader.EndOfStream) {
            _SFXAudioList.Add(Resources.Load<AudioClip>("Sounds/SoundEffects/" + reader.ReadLine()));
        }

        path = "Assets/Resources/Sounds/VoiceLines/voiceLineList.txt";
        reader = new StreamReader(path);
        while (!reader.EndOfStream) {
            _VoiceAudioList.Add(Resources.Load<AudioClip>("Sounds/VoiceLines/" + reader.ReadLine()));
        }

        path = "Assets/Resources/Sounds/BGM/BGMList.txt";
        reader = new StreamReader(path);
        while (!reader.EndOfStream) {
            _BGMAudioList.Add(Resources.Load<AudioClip>("Sounds/BGM/" + reader.ReadLine()));
        }

        path = null;
        reader = null;
    }

    /*****************************************************************
     Instantiates a sound effect. Destroys after finishing.

     Return Type: void
     Parameter: string, Vector3
     Overloaded Function +1
    *****************************************************************/
    public void InstantiateSoundEffect(string soundEffectName, Vector3 pos)
    {
        GameObject _tempObject = null;
        for (int i = 0; i < _SFXAudioList.Count; ++i) {
            if (_SFXAudioList[i].name == soundEffectName) {
                _tempObject = GameObject.Instantiate(Resources.Load("Prefabs/Audio/SFXPlayer"), pos, Quaternion.identity) as GameObject;
                _tempObject.GetComponent<AudioSource>().clip = _SFXAudioList[i];
                _tempObject.GetComponent<AudioSource>().Play();
                break;
            }
        }
    }

    /*****************************************************************
     Plays the sound effect through an already active audiosource,
     and slots the audioclip into it.

     Return Type: void
     Parameter: string, AudioSource
     Overloaded Function +1
    *****************************************************************/
    public void PlaySoundEffect(string soundEffectName, AudioSource _source) {
        for (int i = 0; i < _SFXAudioList.Count; ++i) {
            if (_SFXAudioList[i].name == soundEffectName) {
                if (_source.clip == null || _source.clip.name != soundEffectName)
                _source.clip = _SFXAudioList[i];
                _source.Play();
                break;
            }
        }
    }

    /*****************************************************************
     Creates a new instance of GameObject that has an AudioSource,
     and attach the AudioClip to it. Plays immediately.

     Return Type: void
     Parameter: string, Vector3
     Overloaded Function +1
    *****************************************************************/
    public GameObject PlaySoundEffect(string soundEffectName, Vector3 pos) {
        GameObject _tempObject = null;
        for (int i = 0; i < _SFXAudioList.Count; ++i) {
            if (_SFXAudioList[i].name == soundEffectName) {
                _tempObject = GameObject.Instantiate(Resources.Load("Prefabs/Audio/SFXPlayer"), pos, Quaternion.identity) as GameObject;
                _tempObject.GetComponent<AudioSource>().clip = _SFXAudioList[i];
                _tempObject.GetComponent<AudioSource>().Play();
                break;
            }
        }

        return _tempObject;
    }

    /*****************************************************************
     Creates a new instance of GameObject that has an AudioSource,
     and attach the AudioClip to it. Plays immediately.

     Return Type: void
     Parameter: AudioSource, string, Vector3
     Overloaded Function +2
    *****************************************************************/
    public void PlaySoundEffect(AudioSource _SFX, string soundEffectName, Vector3 pos) {
        if (_SFX.clip != null)
        {
            if (_SFX.clip.name == soundEffectName) {
                if (_SFX.isPlaying)
                {
                    _SFX.Stop();
                }

                _SFX.Play();
                return;
            }
        }

        else {
            for (int i = 0; i < _SFXAudioList.Count; ++i)
            {
                if (_SFXAudioList[i].name == soundEffectName)
                {
                    if (_SFX.isPlaying)
                    {
                        _SFX.Stop();
                    }

                    _SFX.clip = _SFXAudioList[i];
                    _SFX.gameObject.transform.position = new Vector3(pos.x, pos.y, pos.z);
                    _SFX.Play();
                    break;
                }
            }
        }   
    }

    /*****************************************************************
     Assigns an AudioClip to the already active BGM AudioSource.
     If playOnAssign is true, then it starts playing immediately.

     Return Type: void
     Parameter: string, bool, bool
    *****************************************************************/
    public void AssignBackgroundMusic(string title, bool interrupt, bool playOnAssign) {
    #pragma warning disable CS0162 // Unreachable code detected
        for (int i = 0; i < _BGMAudioList.Count; ++i) {
    #pragma warning restore CS0162 // Unreachable code detected
            if (_BGMAudioList[i].name == title && _BGMPlayer.clip != _BGMAudioList[i]) {
                _BGMPlayer.gameObject.GetComponent<BGMHandler>().BGMLength = _BGMAudioList[i].length;
                if (_BGMPlayer.clip != null) {
                    if (interrupt) {
                        _BGMPlayer.Stop();
                        _BGMPlayer.clip = _BGMAudioList[i];
                    }

                }
                else {
                    _BGMPlayer.clip = _BGMAudioList[i];
                }

                if (playOnAssign)
                    _BGMPlayer.Play();

                break;
            }
        }
    }

    /*****************************************************************
     Assigns an AudioClip to the already active BGM AudioSource.
     If playOnAssign is true, then it starts playing with delay.

     Return Type: void
     Parameter: string, bool, bool
    *****************************************************************/
    public void AssignBackgroundMusicDelayed(string title, bool interrupt, bool playOnAssign, ulong delay)
    {
        if (_BGMPlayer.clip != null) {
            if (_BGMPlayer.clip.name == title) {
                if (interrupt) {
                    _BGMPlayer.Stop();
                    _BGMPlayer.gameObject.GetComponent<BGMHandler>().BGMLength = _BGMPlayer.clip.length;

                    if (playOnAssign)
                    {
                        _BGMPlayer.PlayDelayed(delay);
                    }
                }
            }

            else {
                for (int i = 0; i < _BGMAudioList.Count; ++i) {
                    if (_BGMAudioList[i].name == title) {
                        if (interrupt) {
                            _BGMPlayer.Stop();
                            _BGMPlayer.clip = _BGMAudioList[i];
                            _BGMPlayer.gameObject.GetComponent<BGMHandler>().BGMLength = _BGMPlayer.clip.length;

                            if (playOnAssign) {
                                _BGMPlayer.PlayDelayed(delay);
                            }
                        }

                        break;
                    }
                }
            }
        }

        else {
            for (int i = 0; i < _BGMAudioList.Count; ++i) {
                if (_BGMAudioList[i].name == title) {
                    _BGMPlayer.clip = _BGMAudioList[i];
                    _BGMPlayer.gameObject.GetComponent<BGMHandler>().BGMLength = _BGMAudioList[i].length;
                    if (playOnAssign) {
                        _BGMPlayer.PlayDelayed(delay);
                    }

                    break;
                }
            }
        }
    }

    /*****************************************************************
     Plays the BGM if it is currently not playing.

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void PlayBackgroundMusic() {
        if (!_BGMPlayer.isPlaying) {
            _BGMPlayer.Play();
        }
    }

    /*****************************************************************
     Checks if the current BGM has ended.

     Return Type: void
     Parameter: void
    *****************************************************************/
    public bool IsCurrentBGMFinished() {
        bool ended = false;
        if (_BGMPlayer.clip != null) {
            if (_BGMPlayer.GetComponent<BGMHandler>().HasEnded)
            {
                ended = true;
            }
        }
        return ended;
    }

    /*****************************************************************
     Stops the BGM.

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void StopBackgroundMusic() {
        _BGMPlayer.Stop();
    }

    public void StartBGMGradualFadeOff() {
        _BGMPlayer.GetComponent<BGMHandler>().StartGradualFadeOff();
    }

    public bool GradualStopBackgroundMusic(float timer) {
        return _BGMPlayer.GetComponent<BGMHandler>().GradualFadeOff(timer);
    }

    public void StartVoice(Vector3 _pos) {
        _voicePlayer.clip = _VoiceAudioList[0];
        _voicePlayer.GetComponent<VoiceHandler>().BGMLength = _voicePlayer.clip.length;
        _voicePlayer.gameObject.transform.position = new Vector3(_pos.x, _pos.y, _pos.z);
        _voicePlayer.Play();
    }

    /*****************************************************************
     Plays the next Voice

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void PlayNextVoice() {
        if (i_currentVoiceSlot != _VoiceAudioList.Count) {
            ++i_currentVoiceSlot;
        }

        if (_voicePlayer.GetComponent<VoiceHandler>().HasEnded)
        {
            _voicePlayer.clip = _VoiceAudioList[i_currentVoiceSlot];
            _voicePlayer.GetComponent<VoiceHandler>().BGMLength = _voicePlayer.clip.length;
            _voicePlayer.Play();
        }
    }

    /*****************************************************************
     Plays the next Voice with a delay.

     Return Type: void
     Parameter: float
    *****************************************************************/
    public void PlayNextVoiceDelayed(float timer = 0f)
    {
        if (i_currentVoiceSlot != _VoiceAudioList.Count)
        {
            ++i_currentVoiceSlot;            
        }

        if (_voicePlayer.GetComponent<VoiceHandler>().HasEnded)
        {
            _voicePlayer.clip = _VoiceAudioList[i_currentVoiceSlot];
            _voicePlayer.GetComponent<VoiceHandler>().BGMLength = _voicePlayer.clip.length;
            _voicePlayer.PlayDelayed(timer);
        }
    }

    /*****************************************************************
     Plays the next voice and assigns the position of the GameObject.

     Return Type: void
     Parameter: Vector3
    *****************************************************************/
    public void PlayNextVoice(Vector3 _pos) {
        if (i_currentVoiceSlot != _VoiceAudioList.Count) {
            ++i_currentVoiceSlot;

            if (_voicePlayer.isPlaying) {
                _voicePlayer.Stop();
            }

            _voicePlayer.clip = _VoiceAudioList[i_currentVoiceSlot];
            _voicePlayer.GetComponent<VoiceHandler>().BGMLength = _voicePlayer.clip.length;
            _voicePlayer.gameObject.transform.position = new Vector3(_pos.x, _pos.y, _pos.z);
            _voicePlayer.Play();
        }
    }

    /*****************************************************************
     Skips the sequence and find the correct voice through name.
     Plays immediately.

     Return Type: void
     Parameter: string, Vector3
    *****************************************************************/
    public void PlayNextVoice(string name, Vector3 _pos, string namePos) {
        if (i_currentVoiceSlot != _VoiceAudioList.Count) {
            if (_voicePlayer.isPlaying) {
                _voicePlayer.Stop();
            }

            for (int i = 0; i < _VoiceAudioList.Count; ++i) {
                if (_VoiceAudioList[i].name == name) {
                    _voicePlayer.clip = _VoiceAudioList[i];
                    _voicePlayer.GetComponent<VoiceHandler>().BGMLength = _voicePlayer.clip.length;
                    _voicePlayer.gameObject.transform.position = new Vector3(_pos.x, _pos.y, _pos.z);
                    _voicePlayer.Play();
                    break;
                }
            }

            for (int i = 0; i < _VoiceAudioList.Count; ++i) {
                if (_VoiceAudioList[i].name == namePos) {
                    i_currentVoiceSlot = i;
                    break;
                }
            }
        }
    }

    /*****************************************************************
     Plays the voice that you want, without affecting the cycle of
     the voice list.

     Return Type: void
     Parameter: Vector3, string
    *****************************************************************/
    public void PlayVoice(Vector3 pos, string name) {
        if (!_voicePlayer.GetComponent<VoiceHandler>().HasEnded) {
            _voicePlayer.Stop();
        }

        for (int i = 0; i < _VoiceAudioList.Count; ++i) {
            if (_VoiceAudioList[i].name == name) {
                _voicePlayer.clip = _VoiceAudioList[i];
                _voicePlayer.GetComponent<VoiceHandler>().BGMLength = _voicePlayer.clip.length;
                _voicePlayer.gameObject.transform.position = new Vector3(pos.x, pos.y, pos.z);
                _voicePlayer.Play();
            }
        }
    }

    /*****************************************************************
     Plays an audio with delayed time.

     Return Type: void
     Parameter: AudioSource, float
    *****************************************************************/
    public void PlayVoiceDelayed(AudioSource source, float timer = 3.0f) {
        source.PlayDelayed(timer);
    }

    /*****************************************************************
     Repeats the current voice

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void RepeatVoice(float timer = 0f) {
        if (!_voicePlayer.isPlaying) {
            _voicePlayer.GetComponent<VoiceHandler>().BGMLength = _voicePlayer.clip.length;
            _voicePlayer.PlayDelayed(timer);
        }
    }

    /*****************************************************************
     Checks if the voice player has ended

     Return Type: void
     Parameter: void
    *****************************************************************/
    public bool VoiceHasEnded() {
        return _voicePlayer.GetComponent<VoiceHandler>().HasEnded;
    }

    /*****************************************************************
     Checks if the voice player has ended

     Return Type: void
     Parameter: string
     Overloaded +1
    *****************************************************************/
    public bool VoiceHasEnded(string check) {
        bool checker = false;
        if (_voicePlayer.clip.name == check) {
            checker = _voicePlayer.GetComponent<VoiceHandler>().HasEnded;
        }

        return checker;
    }

    public void Restart() {
        if (_BGMPlayer.clip != null) {
            _BGMPlayer.clip = null;
        }
            
        if (_voicePlayer.clip != null) {
            _voicePlayer.clip = null;
        }

        i_currentVoiceSlot = 0;
    }
}