﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPointsBehaviour : MonoBehaviour {
    private bool b_triggered;
    public bool Triggered {
        get { return b_triggered; }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TriggerCollider") {
            if (!b_triggered) {
                b_triggered = true;
            }
        }
    }

    public void ResetTrigger() {
        if (b_triggered)
            b_triggered = false;
    }
}
