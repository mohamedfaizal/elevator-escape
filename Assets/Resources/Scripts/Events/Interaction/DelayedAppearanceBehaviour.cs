﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedAppearanceBehaviour : MonoBehaviour {
    private bool b_observed;
    private bool b_spawn;
    private bool b_lookAt;
    public bool LookingAt {
        get { return b_lookAt; }
    }

    public GameObject _girl;
    public BoxCollider _firstDetection;
    public BoxCollider _secondDetection;
    public GameObject _handprint;

    // Cache
    private Camera _cameraEye;

    private void Awake()
    {      
    }
    // Use this for initialization
    void Start () {
        _cameraEye = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 cameraView = new Vector3(0.5f, 0.5f, 0);
        Ray ray = _cameraEye.ViewportPointToRay(cameraView);
        RaycastHit hit;

        if (!b_observed)
        {
            if (Physics.Raycast(ray, out hit, 50f))
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    b_observed = true;
                    _handprint.SetActive(true);
                }
            }
        }

        else {
            if (!b_spawn)
            {
                if (!Physics.Raycast(ray, out hit, 50f))
                {
                    b_spawn = true;
                    _girl.SetActive(true);
                    _firstDetection.enabled = false;
                    _secondDetection.enabled = true;
                }
            }

            else {
                if (!b_lookAt) {
                    if (Physics.Raycast(ray, out hit, 50f))
                    {
                        if (hit.collider == _secondDetection) {
                            b_lookAt = true;
                        }
                    }
                }
            }
        }
    }
}
