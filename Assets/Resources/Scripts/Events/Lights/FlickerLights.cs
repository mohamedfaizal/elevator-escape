﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

/****************************************************************
 Light manager Script:
 Controls the lights in the game with custom on and off time intervals

 Noel Ho Sing Nam
 noelhosingnam@hotmail.com/ noelhosingnam@viziofly.com
 +65-86111251
 ****************************************************************/

public class FlickerLights : MonoBehaviour {

    [Header("All Light settings")]
    public List<GameObject> lightsGameObject = new List<GameObject>();
    [Range(0,10)]
    [Tooltip("Changes the intensity of all lights in on mode \n( 0 = Do not change )")]
    public float f_onLightIntensity = 1;
    [Range(0, 10)]
    [Tooltip("Changes the intensity of all lights in off mode \n( 0 = Do not change )")]
    public float f_offLightIntensity = 0;
    [Tooltip("Duration which light goes out \n( 0 = Turn off completely )")]
    public float f_flickerDuration = 1.0f;
    [Tooltip("Duration between each light flicker \n( 0 = Never flicker in intervals )")]
    public float f_flickerInterval = 5.0f;
    [Tooltip("Time until first light flicker \n( 0 = No initial flicker )")]
    public float f_initialFlicker = 0.0f;
    [Tooltip("Create custom flicker pattern \n( 0 = Use normal interval )")]
    public List<float> _customFlickerInterval;
    [Tooltip("Custom flicker pattern for event trigger")]
    public List<float> _eventFlickerInterval;

    // On and Off Timers
    private float f_offTimer = 0.0f;
    private float f_onTimer = 0.0f;

    // Custom flicker pattern number
    private int i_customFlickerCounter = 0;

    // Event flicker event
    private bool b_eventFlickering = false;

    // Stop regular flickering
    private bool b_stopFlickeringInterval = false;

    public List<TextAsset> _eventList = new List<TextAsset>();
    private List<List<float>> _eventListIntervals = new List<List<float>>();

    private int i_currEventId = -1;

    /*****************************************************************
     Use this for initialization

     Return Type: void
     Parameter: void
    *****************************************************************/
    void Start ()
    {
        // Change all light intensities
		if(f_onLightIntensity != 0)
        {
            setAllLightIntensity(f_onLightIntensity);
        }
        
        // Set initial flicker time, else set to regular interval
        if(f_initialFlicker != 0)
        {
           f_onTimer = f_initialFlicker;
        }
        else if(f_flickerInterval != 0)
        {
            f_onTimer = f_flickerInterval;
        }

        for(int i = 0; i < _eventList.Count; i++)
        {
            string s_placeHolder = _eventList[i].text;
            string[] s_placeHolderArray = s_placeHolder.Split(',');

            List<float> _eventListIntervalsInt = new List<float>();
            for (int j = 0; j < s_placeHolderArray.Length; j++)
            {
                _eventListIntervalsInt.Add(float.Parse(s_placeHolderArray[j]));
            }
            _eventListIntervals.Add(_eventListIntervalsInt);
        }
	}

    /*****************************************************************
     Update is called once per frame

     Return Type: void
     Parameter: void
    *****************************************************************/
    void Update ()
    {
        if(b_eventFlickering == true)
        {
            if (i_currEventId == -1)
                eventFlicker();
            else
                eventFlicker(i_currEventId);
        }
        else if (b_stopFlickeringInterval != true)
        {
            CountDownTimer();
        }

        // Keyboard debugging
        /*
		if(Input.GetKeyDown(KeyCode.A))
        {
            turnOnAllLights();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            turnOffAllLights();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            flickerNowOff();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            flickerNowOn();
        }
        if(Input.GetKeyDown(KeyCode.G))
        {
            triggerEventFlicker(true, true);
        }
        */
    }

    /*****************************************************************
     Activates event flickering pattern

     Return Type: void
     Parameter: bool, bool, int

     stopInterval forces the lights to stay on/off after the event
     offLightsFirst turns off the light as the first event action
     eventId picks which event to play
    *****************************************************************/
    public void triggerEventFlicker(bool stopInterval = true, bool offLightsFirst = true, int eventId = -1)
    {
        if(eventId != -1 && eventId >= _eventListIntervals.Count)
        {
            Debug.Log("Error: There isn\'t a " + eventId + "th lights event txt file >:c");
            return;
        }

        if (b_eventFlickering == false)
        {
            i_currEventId = eventId;
            i_customFlickerCounter = 0;
            f_offTimer = 0.0f;
            f_onTimer = 0.0f;

            if (stopInterval)
            {
                stopFlickerInterval();
            }

            if (offLightsFirst == true)
            {
                turnOffAllLights();
                if(eventId == -1)
                    f_offTimer = _eventFlickerInterval[i_customFlickerCounter];
                else
                    f_offTimer = _eventListIntervals[eventId][i_customFlickerCounter];
                i_customFlickerCounter++;
            }
            else
            {
                turnOnAllLights();
                if(eventId == -1)
                    f_onTimer = _eventFlickerInterval[i_customFlickerCounter];
                else
                    f_offTimer = _eventListIntervals[eventId][i_customFlickerCounter];
                i_customFlickerCounter++;
            }

            b_eventFlickering = true;
        }
    }

    /*****************************************************************
     Stops regular flickering

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void stopFlickerInterval()
    {
        b_stopFlickeringInterval = true;
    }

    /*****************************************************************
     Forces interval to be in off state

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void flickerNowOff()
    {
        turnOffAllLights();
        f_offTimer = f_flickerDuration;
        f_onTimer = 0.0f;
    }

    /*****************************************************************
     Forces interval to be in on state

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void flickerNowOn()
    {
        turnOnAllLights();
        f_offTimer = 0.0f;
        f_onTimer = f_flickerInterval;
    }

    /*****************************************************************
     Turn on and off at intervals

     Return Type: void
     Parameter: void
    *****************************************************************/
    private void CountDownTimer()
    {
        // If lights are currently off
        if (f_offTimer > 0.0f)
        {
            f_offTimer -= Time.deltaTime;
            if (f_offTimer <= 0.0f)
            {
                f_offTimer = 0.0f;
                turnOnAllLights();
                if (f_flickerInterval != 0)
                {
                    f_onTimer = f_flickerInterval;

                    // If a custom light interval is set
                    if (_customFlickerInterval.Count > 0)
                    {
                        if (i_customFlickerCounter == _customFlickerInterval.Count)
                        {
                            i_customFlickerCounter = 0;
                        }

                        f_onTimer = _customFlickerInterval[i_customFlickerCounter];
                        i_customFlickerCounter++;
                    }
                }
            }
        }
        else if (f_onTimer > 0.0f)
        {
            f_onTimer -= Time.deltaTime;
            if (f_onTimer <= 0.0f)
            {
                f_onTimer = 0.0f;
                turnOffAllLights();
                if (f_flickerDuration != 0)
                {
                    f_offTimer = f_flickerDuration;

                    // If a custom light interval is set
                    if (_customFlickerInterval.Count > 0)
                    {
                        if (i_customFlickerCounter == _customFlickerInterval.Count)
                        {
                            i_customFlickerCounter = 0;
                        }

                        f_offTimer = _customFlickerInterval[i_customFlickerCounter];
                        i_customFlickerCounter++;
                    }
                }
            }
        }
    }

    /*****************************************************************
     Turn on and off according to event intervals

     Return Type: void
     Parameter: void
    *****************************************************************/
    private void eventFlicker()
    {
        // If lights are currently off
        if (f_offTimer > 0.0f)
        {
            f_offTimer -= Time.deltaTime;
            if (f_offTimer <= 0.0f)
            {
                f_offTimer = 0.0f;
                turnOnAllLights();

                if (_eventFlickerInterval.Count > 0)
                {
                    if (i_customFlickerCounter == _eventFlickerInterval.Count)
                    {
                        i_customFlickerCounter = 0;
                        b_eventFlickering = false;
                    }

                    f_onTimer = _eventFlickerInterval[i_customFlickerCounter];
                    i_customFlickerCounter++;
                }
            }
        }
        else if (f_onTimer > 0.0f)
        {
            f_onTimer -= Time.deltaTime;
            if (f_onTimer <= 0.0f)
            {
                f_onTimer = 0.0f;
                turnOffAllLights();

                if (_eventFlickerInterval.Count > 0)
                {
                    if (i_customFlickerCounter == _eventFlickerInterval.Count)
                    {
                        i_customFlickerCounter = 0;
                        b_eventFlickering = false;
                    }

                    f_offTimer = _eventFlickerInterval[i_customFlickerCounter];
                    i_customFlickerCounter++;
                }
            }
        }
    }

    /*****************************************************************
     Turn on and off according to event intervals in txt files

     Return Type: void
     Parameter: int
    *****************************************************************/
    private void eventFlicker(int eventId)
    {
        // If lights are currently off
        if (f_offTimer > 0.0f)
        {
            f_offTimer -= Time.deltaTime;
            if (f_offTimer <= 0.0f)
            {
                f_offTimer = 0.0f;
                turnOnAllLights();

                if (_eventListIntervals[eventId].Count > 0)
                {
                    if (i_customFlickerCounter == _eventListIntervals[eventId].Count)
                    {
                        i_customFlickerCounter = 0;
                        b_eventFlickering = false;
                    }

                    f_onTimer = _eventListIntervals[eventId][i_customFlickerCounter];
                    i_customFlickerCounter++;
                }
            }
        }
        else if (f_onTimer > 0.0f)
        {
            f_onTimer -= Time.deltaTime;
            if (f_onTimer <= 0.0f)
            {
                f_onTimer = 0.0f;
                turnOffAllLights();

                if (_eventListIntervals[eventId].Count > 0)
                {
                    if (i_customFlickerCounter == _eventListIntervals[eventId].Count)
                    {
                        i_customFlickerCounter = 0;
                        b_eventFlickering = false;
                    }

                    f_offTimer = _eventListIntervals[eventId][i_customFlickerCounter];
                    i_customFlickerCounter++;
                }
            }
        }
    }

    /*****************************************************************
     Changes the intensity of all lights

     Return Type: void
     Parameter: float
    *****************************************************************/
    public void setAllLightIntensity(float lightIntensity)
    {
        for (int i = 0; i < lightsGameObject.Count; i++)
        {
            lightsGameObject[i].GetComponent<Light>().intensity = lightIntensity;
        }
    }

    /*****************************************************************
     Turn on all lights

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void turnOnAllLights()
    {
        setAllLightIntensity(f_onLightIntensity);
    }

    /*****************************************************************
     Turn off all lights

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void turnOffAllLights()
    {
        setAllLightIntensity(f_offLightIntensity);
    }

    /*****************************************************************
     Returns event status

     Return Type: bool
     Parameter: void
    *****************************************************************/
    public bool checkIfEventActive()
    {
        return b_eventFlickering;
    }

    /*****************************************************************
     Returns event id

     Return Type: bool
     Parameter: void
    *****************************************************************/
    public int getEventId()
    {
        return i_currEventId;
    }
}
