﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Light manager Script:
 Controls the lights in the game with custom on and off time intervals

 Noel Ho Sing Nam
 noelhosingnam@hotmail.com/ noelhosingnam@viziofly.com
 +65-86111251
 ****************************************************************/

public class LightManager : MonoBehaviour {

    /*****************************************************************
     Lighting class

     Controls the properties of the light and effects
    *****************************************************************/
    [System.Serializable]
    public class Lighting
    {
        [Header("All Light settings")]
        public Light _light;
        [Tooltip("Sets the intial light on (true) or off (false)")]
        public bool b_onLight = true;
        [Tooltip("Delay between command sent and received")]
        public float f_delay;
        [Tooltip("Additional random delay to make it more random")]
        public float f_randomDelay = 0.0f;
        [Tooltip("Intensity of the light when it is on")]
        public float f_onIntensity = 1.0f;
        [Tooltip("How long light on lasts until it turns back to flickering")]
        public float f_onFreezeTime = 1.0f;
        [Tooltip("How long light off lasts until it turns back to flickering")]
        public float f_offFreezeTime = 1.0f;

        [Tooltip("Sets the flickering pattern")]
        public List<float> _customFlickerInterval;
        [HideInInspector]
        public List<float> _eventFlickerInterval;

        [HideInInspector]
        public float f_managerDelay;

        [HideInInspector]
        public float f_frequency;

        public float f_offTimer = 0.01f;
        public float f_onTimer = 0.00f;
        public float f_freezeTimer = 0.00f;
        public float f_frozenTimer = 0.00f;
        public float f_eventTimer = 0.00f;
        public float f_fadeRate = 0.00f;
        public float f_setOnIntensity = 1.00f;
        public int i_customFlickerIntervalTracker = 0;
        public bool b_isOn;
        public bool b_init = false;

        /*****************************************************************
         Initializes certain varialbes that require to be set in inspector first

         Return Type: void
         Parameter: void
        *****************************************************************/
        public void Initialize()
        {
            if(b_onLight == true)
            {
                TurnOn();
                f_offTimer = 0.00f;
                f_onTimer = 0.01f;
            }
            else
            {
                TurnOff();
                f_offTimer = 0.01f;
                f_onTimer = 0.00f;
            }

            f_setOnIntensity = f_onIntensity;
            b_init = true;
        }

        /*****************************************************************
         Update is called once per frame

         Return Type: void
         Parameter: void
        *****************************************************************/
        public void Update()
        {
            if(b_init == false)
            {
                Initialize();
            }

            CountDownTimer();
            if(f_fadeRate != 0.00f)
            {
                AddMaxIntensity(f_fadeRate);
                if ((f_fadeRate < 0.00f && f_onIntensity <= 0.00f) || (f_fadeRate > 0.00f && f_onIntensity >= f_setOnIntensity))
                {
                    f_fadeRate = 0.00f;
                }
            }
        }

        /*****************************************************************
         Countdowns all the timer for flickering

         Return Type: void
         Parameter: void
        *****************************************************************/
        private void CountDownTimer()
        {
            // Time until turn on if SwitchOn is called
            if(f_freezeTimer > 0.00f)
            {
                f_freezeTimer -= 1.0f * Time.deltaTime;
                if(f_freezeTimer <= 0.00f)
                {
                    TurnOn();
                    f_frozenTimer = f_onFreezeTime;
                }
            }

            if (f_frozenTimer <= 0.00f)
            {
                if (_customFlickerInterval.Count > 0)
                {
                    if (f_offTimer > 0.00f)
                    {
                        f_offTimer -= Time.deltaTime;
                        if (f_offTimer <= 0.00f)
                        {
                            if (i_customFlickerIntervalTracker >= _customFlickerInterval.Count)
                            {
                                i_customFlickerIntervalTracker = 0;
                            }

                            f_offTimer = 0.00f;
                            TurnOn();
                            f_onTimer = _customFlickerInterval[i_customFlickerIntervalTracker] + 0.01f;
                            i_customFlickerIntervalTracker++;
                        }
                    }
                    else if (f_onTimer > 0.00f)
                    {
                        f_onTimer -= Time.deltaTime;
                        if (f_onTimer <= 0.00f)
                        {
                            if (i_customFlickerIntervalTracker >= _customFlickerInterval.Count)
                            {
                                i_customFlickerIntervalTracker = 0;
                            }

                            f_onTimer = 0.00f;
                            TurnOff();
                            f_offTimer = _customFlickerInterval[i_customFlickerIntervalTracker] + 0.01f;
                            i_customFlickerIntervalTracker++;
                        }
                    }
                }
            }
            else if(f_frozenTimer > 0.00f)
            {
                // Flicker interval is frozen if a switchon or switchoff is called
                f_frozenTimer -= 1.0f * Time.deltaTime;
                if(f_frozenTimer <= 0.00f)
                {
                    if(f_offTimer > 0.00f)
                    {
                        TurnOff();
                    }
                    else if(f_onTimer > 0.00f)
                    {
                        TurnOn();
                    }
                }
            }

            // Timer for event flicker pattern
            if(f_eventTimer > 0.00f)
            {
                f_eventTimer -= 1.0f * Time.deltaTime;
                if(f_eventTimer <= 0.00f)
                {
                    i_customFlickerIntervalTracker = 0;
                    f_offTimer = 0.00f;
                    f_onTimer = 0.01f;
                    _customFlickerInterval = _eventFlickerInterval;
                }
            }
        }

        /*****************************************************************
         Sets the intensity of the light ( will get overwritten by flickering )

         Return Type: void
         Parameter: float
        *****************************************************************/
        public void SetIntensity(float newIntensity)
        {
            _light.intensity = newIntensity;
        }

        /*****************************************************************
         Adds the intensity of the light ( will get overwritten by flickering )

         Return Type: void
         Parameter: float
        *****************************************************************/
        public void AddIntensity(float diffIntensity)
        {
            _light.intensity += diffIntensity;
        }

        /*****************************************************************
         Sets the maxintensity of the light ( will NOT get overwritten by flickering )

         Return Type: void
         Parameter: float
        *****************************************************************/
        public void SetMaxIntensity(float newIntensity)
        {
            f_onIntensity = newIntensity;
            if(_light.intensity >= f_onIntensity)
            {
                SetIntensity(f_onIntensity);
            }
        }

        /*****************************************************************
         Adds the maxintensity of the light ( will NOT get overwritten by flickering )

         Return Type: void
         Parameter: float
        *****************************************************************/
        public void AddMaxIntensity(float diffIntesnity)
        {
            f_onIntensity += diffIntesnity;

            if(b_isOn == true)
            {
                SetIntensity(f_onIntensity);
            }

            if(_light.intensity >= f_onIntensity)
            {
                SetIntensity(f_onIntensity);
            }
        }

        /*****************************************************************
         Resets the max intenisty to the one set in the inspector

         Return Type: void
         Parameter: void
        *****************************************************************/
        public void ResetMaxIntensity()
        {
            f_onIntensity = f_setOnIntensity;
        }

        /*****************************************************************
         Switches on the light for f_onFreezeTime ( with delay )

         Return Type: void
         Parameter: void
        *****************************************************************/
        public void SwitchOn()
        {
            ResetMaxIntensity();
            f_fadeRate = 0.00f;
            f_freezeTimer = f_managerDelay + f_delay + Random.Range(-f_randomDelay, f_randomDelay) + 0.01f;
        }

        /*****************************************************************
         Switches off the light for f_offFreezeTime

         Return Type: void
         Parameter: void
        *****************************************************************/
        public void SwitchOff()
        {
            ResetMaxIntensity();
            TurnOff();
            f_fadeRate = 0.00f;
            f_frozenTimer = f_offFreezeTime;
        }

        /*****************************************************************
         Switches on the light instantly

         Return Type: void
         Parameter: void
        *****************************************************************/
        public void SwitchOnInstant()
        {
            ResetMaxIntensity();
            f_fadeRate = 0.00f;
            SetIntensity(f_onIntensity);
            b_isOn = true;
        }

        /*****************************************************************
         Switches off the light instantly

         Return Type: void
         Parameter: void
        *****************************************************************/
        public void SwitchOffInstant()
        {
            ResetMaxIntensity();
            f_fadeRate = 0.00f;
            SetIntensity(0.00f);
            b_isOn = false;
        }

        /*****************************************************************
         Switches on the light

         Return Type: void
         Parameter: void
        *****************************************************************/
        private void TurnOn()
        {
            SetIntensity(f_onIntensity);
            b_isOn = true;
        }

        /*****************************************************************
         Switches off the light

         Return Type: void
         Parameter: void
        *****************************************************************/
        private void TurnOff()
        {
            SetIntensity(0.0f);
            b_isOn = false;
        }

        /*****************************************************************
         Swaps the current flickering pattern with an event pattern

         Return Type: void
         Parameter: List<float>, float
        *****************************************************************/
        public void ActivateEvent(List<float> eventPattern, float eventDuration = 5.0f)
        {
            if (f_eventTimer <= 0.00f)
            {
                f_eventTimer = eventDuration;
                f_offTimer = 0.00f;
                f_onTimer = 0.01f;
                i_customFlickerIntervalTracker = 0;
                _eventFlickerInterval = _customFlickerInterval;
                _customFlickerInterval = eventPattern;
            }
        }

        /*****************************************************************
         Fades out the light

         Return Type: void
         Parameter: float
        *****************************************************************/
        public void FadeOut(float fadeDuration = 5.0f)
        {
            Mathf.Clamp(fadeDuration, 0.01f, 999f);
            f_fadeRate = -f_setOnIntensity / fadeDuration * Time.deltaTime;
            //f_fadeRate = -((_light.intensity / fadeDuration) * Time.deltaTime);
        }

        /*****************************************************************
         Fades in the light

         Return Type: void
         Parameter: float
        *****************************************************************/
        public void FadeIn(float fadeDuration = 5.0f)
        {
            Mathf.Clamp(fadeDuration, 0.01f, 999f);
            f_fadeRate = f_setOnIntensity / fadeDuration * Time.deltaTime;
            //f_fadeRate = ((f_setOnIntensity - _light.intensity) / fadeDuration) * Time.deltaTime;
        }

        /*****************************************************************
         Returns whether the light is on

         Return Type: bool
         Parameter: void
        *****************************************************************/
        public bool GetIsOn()
        {
            if(_light.intensity <= 0.0f)
            {
                b_isOn = false;
            }
            else if(_light.intensity >= f_onIntensity)
            {
                b_isOn = true;
            }

            return b_isOn;
        }
    }

    public List<Lighting> _lights = new List<Lighting>();
    [Tooltip("Increases delay by distance between lights")]
    public bool b_distanceDelay = false;
    [Tooltip("If b_distanceDelay is true, scale the delay")]
    public float b_distanceDelayMultiplier = 0.5f;
    [Tooltip("Adds a standarad delay between lights")]
    public float f_delayBetweenLights = 0.0f;

    [Tooltip("Add custom patterns here to call as event later")]
    public List<string> _customPatternsInspector = new List<string>();
    public List<List<float>> _customPatterns = new List<List<float>>();

    [HideInInspector]
    public List<float> _distanceDelay = new List<float>();

    /*****************************************************************
     Use this for initialization

     Return Type: void
     Parameter: void
    *****************************************************************/
    void Start () {
        // If add extra delay based on distance is true, calculate the delay for each object
		if(b_distanceDelay == true && _lights.Count >= 1)
        {
            // Adds the first distance between the first light and the location where this script is placed
            _distanceDelay.Add(f_delayBetweenLights + Vector3.Distance(this.transform.position, _lights[0]._light.transform.position) * b_distanceDelayMultiplier);
            for(int i = 1; i < _lights.Count; i++)
            {
                // Adds distance between i and i-1 lights
                _distanceDelay.Add(f_delayBetweenLights + Vector3.Distance(_lights[i]._light.transform.position, _lights[i - 1]._light.transform.position) * b_distanceDelayMultiplier);
            }
        }

        for(int i = 0; i < _lights.Count; i++)
        {
            if(b_distanceDelay == true)
            {
                // Passes the distance delay into the lights
                _lights[i].f_managerDelay = _distanceDelay[i];
            }
            else
            {
                // Passes a standard delay to all lights
                _lights[i].f_managerDelay = f_delayBetweenLights;
            }
        }

        // Converts the pattern string into float
        for(int i = 0; i < _customPatternsInspector.Count; i++)
        {
            List<float> f_placeHolder = new List<float>();
            string[] s_placeHolder = _customPatternsInspector[i].Split(',');
            for(int j = 0; j < s_placeHolder.Length; j++)
            {
                f_placeHolder.Add(float.Parse(s_placeHolder[j]));
            }
            _customPatterns.Add(f_placeHolder);
        }
	}

    /*****************************************************************
     Update is called every frame

     Return Type: void
     Parameter: void
    *****************************************************************/
    void Update () {
        if(Input.GetKeyDown(KeyCode.W))
        {
            for(int i = 0; i < _lights.Count; i++)
            {
                _lights[i].ActivateEvent(_customPatterns[0]);
            }
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            for(int i = 0; i < _lights.Count; i++)
            {
                _lights[i].SwitchOff();
            }
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            for(int i = 0; i < _lights.Count; i++)
            {
                _lights[i].SwitchOn();
            }
        }
        if(Input.GetKeyDown(KeyCode.T))
        {
            for(int i = 0; i < _lights.Count; i++)
            {
                _lights[i].FadeOut(1);
            }
        }
        if(Input.GetKeyDown(KeyCode.Y))
        {
            for(int i = 0; i < _lights.Count; i++)
            {
                _lights[i].FadeIn(1);
            }
        }

		for(int i = 0; i < _lights.Count; i++)
        {
            _lights[i].Update();
        }
	}

    /*****************************************************************
     Sets Light intensity

     Return Type: void
     Parameter: int, float
    *****************************************************************/
    public void SetLightIntensity(int id, float lightIntensity)
    {
        _lights[id].SetIntensity(lightIntensity);
    }

    /*****************************************************************
     Sets light intensity of all lights

     Return Type: void
     Parameter: float
    *****************************************************************/
    public void SetAllLightIntensity(float lightIntensity)
    {
        for(int i = 0; i < _lights.Count; i++)
        {
            SetLightIntensity(i, lightIntensity);
        }
    }

    /*****************************************************************
     Turns on light ( with delay )

     Return Type: void
     Parameter: int
    *****************************************************************/
    public void TurnOnLight(int id)
    {
        _lights[id].SwitchOn();
    }

    /*****************************************************************
     Turns on all light ( with delay )

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void TurnOnAllLights()
    {
        for(int i = 0; i < _lights.Count; i++)
        {
            TurnOnLight(i);
        }
    }

    /*****************************************************************
     Turns off light

     Return Type: void
     Parameter: int
    *****************************************************************/
    public void TurnOffLight(int id)
    {
        _lights[id].SwitchOff();
    }

    /*****************************************************************
     Turns off all light

     Return Type: void
     Parameter: void
    *****************************************************************/
    public void TurnOffAllLights()
    {
        for(int i = 0; i < _lights.Count; i++)
        {
            TurnOffLight(i);
        }
    }
}
