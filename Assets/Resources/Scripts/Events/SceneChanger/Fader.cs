﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/****************************************************************
 Fader
 Usage:
 Smoother transition. Attach this script to an Image with
 a color of your choice.

 Description:
 A smoother transitions between scenes. (Only for additive scene)

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/
public enum FadeState
{
    Standby,
    InstantFadeIn,
    FadeIn,
    InstantFadeOut,
    FadeOut
}

public class Fader : MonoBehaviour {
    public Image Background;
    public const float f_timer = 1f;

    private float f_fadeTime;
    private const float f_fadeInTime = 1.0f;
    private const float f_adjustTime = 0.5f;
    private const float f_fadeOutTime = 0.0f;

    private FadeState _currentFadeState;

    public bool Standby {
        get { return _currentFadeState == FadeState.Standby; }
    }

    public FadeState FadeState {
        get { return _currentFadeState; }
    }

    // Cache
    private CanvasGroup _faderCanvasGroup;

    private void Awake()
    {
        _faderCanvasGroup = gameObject.GetComponent<CanvasGroup>();
        f_fadeTime = _faderCanvasGroup.alpha;
        _currentFadeState = FadeState.Standby;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        switch (_currentFadeState) {
            case FadeState.Standby:
                break;

            case FadeState.InstantFadeIn:
                InstantFadeIn();
                break;

            case FadeState.FadeIn:
                GradualFadeIn();
                break;

            case FadeState.InstantFadeOut:
                InstantFadeOut();
                break;

            case FadeState.FadeOut:
                StartCoroutine(GradualFadeOut());
                break;

            default:
                break;
        }
	}

    public void ToStandby() {
        _currentFadeState = FadeState.Standby;
    }

    public void ToInstantFadeIn() {
        _currentFadeState = FadeState.InstantFadeIn;
    }

    public void ToFadeIn() {
        _currentFadeState = FadeState.FadeIn;
    }

    public void ToInstantFadeOut() {
        _currentFadeState = FadeState.InstantFadeOut;
    }

    public void ToFadeOut() {
        _currentFadeState = FadeState.FadeOut;
    }

    private void InstantFadeIn() {
        _faderCanvasGroup.alpha = f_fadeInTime;
        f_fadeTime = f_fadeInTime;
        _currentFadeState = FadeState.Standby;
    }

    private void GradualFadeIn() {
        f_fadeTime += f_timer * Time.deltaTime;
        _faderCanvasGroup.alpha = f_fadeTime;
        if (f_fadeTime >= f_fadeInTime) {
            f_fadeTime = f_fadeInTime;
            _faderCanvasGroup.alpha = f_fadeInTime;
            _currentFadeState = FadeState.Standby;
        }
    }

    private void InstantFadeOut() {
        _faderCanvasGroup.alpha = f_fadeOutTime;
        f_fadeTime = f_fadeOutTime;
        _currentFadeState = FadeState.Standby;
    }

    private IEnumerator GradualFadeOut() {
        yield return new WaitForSeconds(f_timer);
        f_fadeTime -= f_timer * Time.deltaTime;
        _faderCanvasGroup.alpha = f_fadeTime;
        if (f_fadeTime <= f_fadeOutTime) {
            f_fadeTime = f_fadeOutTime;
            _faderCanvasGroup.alpha = f_fadeOutTime;
            _currentFadeState = FadeState.Standby;
        }
    }

    public void ActiveSplashPage(bool active) {
        Background.gameObject.SetActive(active);
    }
}
