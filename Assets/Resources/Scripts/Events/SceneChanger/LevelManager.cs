﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/****************************************************************
 Input behaviour
 Usage:
 Singleton script. Use it by calling the GetInstance function;

 Description:
 Backend technical support for scene loading and unloading. Applicable
 to ONLY additive scene loading.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/
public class LevelManager {
    private static LevelManager _levelManager = null;
    private Scene _nextScene;

    public static LevelManager GetInstance() {
        if (_levelManager == null) {
            _levelManager = new LevelManager();
        }

        return _levelManager;
    }

    /*****************************************************************
     Checks if the current scene is loaded.

     Return Type: bool
     Parameter: void
    *****************************************************************/
    public bool SceneIsLoaded() {
        return (_nextScene.isLoaded);
    }

    /*****************************************************************
     Checks if the current scene is loaded. Overloaded function.

     Return Type: bool
     Parameter: string
    *****************************************************************/
    public bool SceneIsLoaded(string sceneName) {
        return SceneManager.GetSceneByName(sceneName).isLoaded;
    }

    /*****************************************************************
     Loads a new additive level.

     Return Type: IEnumerator
     Parameter: string, bool
    *****************************************************************/
    public IEnumerator LoadLevel(string sceneName, bool unloadOthers) {
        yield return new WaitForEndOfFrame();
        SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        _nextScene = SceneManager.GetSceneByName(sceneName);
        if (unloadOthers) {
            for (int i = 0; i < SceneManager.sceneCount; ++i) {
                if (SceneManager.GetSceneAt(i).name != sceneName && SceneManager.GetSceneAt(i).name != "MasterScene")
                {
                    SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
                }
            }
        }
    }

    public IEnumerator UnloadLevel() {
        yield return new WaitForEndOfFrame();

        for (int i = 0; i < SceneManager.sceneCount; ++i) {
            if (SceneManager.GetSceneAt(i).name != "MasterScene")
            {
                SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
            }
        }
    }

    /*****************************************************************
     Unloads a level.

     Return Type: IEnumerator
     Parameter: string
    *****************************************************************/
    public IEnumerator UnloadLevel(string exception) {
        yield return new WaitForEndOfFrame();

        for (int i = 0; i < SceneManager.sceneCount; ++i) {
            if (SceneManager.GetSceneAt(i).name != exception && SceneManager.GetSceneAt(i).name != "MasterScene")
            {
                SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
            }
        }
    }
}