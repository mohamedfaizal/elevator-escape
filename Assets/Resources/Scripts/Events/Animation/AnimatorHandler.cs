﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ANIMATION_TYPE {
    ANIMATION_TYPE_STANDBY,
    ANIMATION_TYPE_IDLE,
    ANIMATION_TYPE_RUN,
    ANIMATION_TYPE_WAVE,
    ANIMATION_TYPE_END
}

public class AnimatorHandler : MonoBehaviour {
    [Tooltip("Attach the same GameObject's animator to this.")]
    public Animator _thisAnimator;

    [Range(1f, 3f)]
    [Tooltip("The duration in Idle state.")]
    public float f_idleTime = 3.0f;

    [Range(1f, 3f)]
    [Tooltip("The duration in running state.")]
    public float f_runTime = 3.0f;

    [Range(1f, 5f)]
    [Tooltip("How fast the girl runs per sec")]
    public float f_speed = 1f;

    // Cache
    private ANIMATION_TYPE _ANIMATION_TYPE;

    public bool AnimationEnd {
        get { return _ANIMATION_TYPE == ANIMATION_TYPE.ANIMATION_TYPE_END; }
    }

    private float f_timer;
    private const float f_time = 1.0f;

    private void Awake()
    {
        _ANIMATION_TYPE = ANIMATION_TYPE.ANIMATION_TYPE_STANDBY;
        f_timer = 0f;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        switch (_ANIMATION_TYPE) {
            case ANIMATION_TYPE.ANIMATION_TYPE_STANDBY:
                break;

            case ANIMATION_TYPE.ANIMATION_TYPE_IDLE:
                Idle();
                break;

            case ANIMATION_TYPE.ANIMATION_TYPE_RUN:
                Running();
                break;

            case ANIMATION_TYPE.ANIMATION_TYPE_WAVE:
                Waving();
                break;

            case ANIMATION_TYPE.ANIMATION_TYPE_END:
                break;

            default:
                break;
        }
    }

    public void ToRunning() {
        _thisAnimator.SetBool("Running", true);
        _ANIMATION_TYPE = ANIMATION_TYPE.ANIMATION_TYPE_RUN;
    }

    public void ToIdle() {
        _thisAnimator.SetBool("Running", false);
        _thisAnimator.SetBool("Waving", false);
        _ANIMATION_TYPE = ANIMATION_TYPE.ANIMATION_TYPE_IDLE;
    }

    public void ToWave() {
        _thisAnimator.SetBool("Waving", true);
        _ANIMATION_TYPE = ANIMATION_TYPE.ANIMATION_TYPE_WAVE;
    }

    public void Restart() {
        if (_ANIMATION_TYPE == ANIMATION_TYPE.ANIMATION_TYPE_END) {
            f_timer = 0f;
            _ANIMATION_TYPE = ANIMATION_TYPE.ANIMATION_TYPE_STANDBY;
        }
    }

    private void Running() {
        f_timer += f_time * Time.deltaTime;
        if (f_timer < f_runTime) {
            transform.position += Vector3.forward * f_speed * Time.deltaTime;
        }

        else {
            _thisAnimator.SetBool("Running", false);
            _ANIMATION_TYPE = ANIMATION_TYPE.ANIMATION_TYPE_END;
        }
    }

    private void Waving() {
        f_timer += f_time * Time.deltaTime;

        if (f_timer > 2f) {
            _thisAnimator.SetBool("Waving", false);
            _ANIMATION_TYPE = ANIMATION_TYPE.ANIMATION_TYPE_END;
        }
    }

    private void Idle() {
        f_timer += f_time * Time.deltaTime;
        if (f_timer > f_idleTime) {
            _ANIMATION_TYPE = ANIMATION_TYPE.ANIMATION_TYPE_END;
        }
    }
}
