﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************************
 Lift Door Behaviour
 Usage:
 Updates the Lift door's behaviour. Attach is script to an empty
 GameObject with doors as child.

 Description:
 Updates the lift door's behaviour.

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/
public enum ELEVATOR_DOOR_STATE
{
    EVELATOR_DOOR_STATE_STANDBY,
    ELEVATOR_DOOR_STATE_OPEN,
    ELEVATOR_DOOR_STATE_CLOSE,
    ELEVATOR_DOOR_STATE_END
}

public class ElevatorDoorBehaviour : MonoBehaviour {
    public GameObject _leftDoor;
    public GameObject _rightDoor;

    private ELEVATOR_DOOR_STATE _ELEVATOR_DOOR_STATE;
    public float f_doorMovementTime;

    private bool b_isOpen;
    private bool b_doorIsMoving;
    private float f_originalPosX;

    // Get and Set methods
    public ELEVATOR_DOOR_STATE ElevatorDoorState
    {
        get { return _ELEVATOR_DOOR_STATE; }
    }

    public bool DoorIsOpen
    {
        get { return b_isOpen; }
    }

    public bool DoorIsMoving
    {
        get { return b_doorIsMoving; }
    }

    private void Awake()
    {
        b_isOpen = false;
        b_doorIsMoving = false;
        f_originalPosX = transform.position.x;
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        switch (_ELEVATOR_DOOR_STATE)
        {
            case ELEVATOR_DOOR_STATE.EVELATOR_DOOR_STATE_STANDBY:
                break;

            case ELEVATOR_DOOR_STATE.ELEVATOR_DOOR_STATE_OPEN:
                OpenDoor();
                break;

            case ELEVATOR_DOOR_STATE.ELEVATOR_DOOR_STATE_CLOSE:
                CloseDoor();
                break;


            case ELEVATOR_DOOR_STATE.ELEVATOR_DOOR_STATE_END:
                break;

            default:
                break;
        }
    }

    public void ToDoorStandby()
    {
        _ELEVATOR_DOOR_STATE = ELEVATOR_DOOR_STATE.EVELATOR_DOOR_STATE_STANDBY;
        b_doorIsMoving = false;
    }

    public void ToOpenDoorState()
    {
        _ELEVATOR_DOOR_STATE = ELEVATOR_DOOR_STATE.ELEVATOR_DOOR_STATE_OPEN;
        b_doorIsMoving = true;
    }

    public void ToCloseDoorState()
    {
        if (!b_doorIsMoving)
        {
            _ELEVATOR_DOOR_STATE = ELEVATOR_DOOR_STATE.ELEVATOR_DOOR_STATE_CLOSE;
            b_doorIsMoving = true;

        }
    }

    private void OpenDoor()
    {
        if (!b_isOpen)
        {
            _leftDoor.transform.position = new Vector3(_leftDoor.transform.position.x + f_doorMovementTime * Time.deltaTime, _leftDoor.transform.position.y, _leftDoor.transform.position.z);
            _rightDoor.transform.position = new Vector3(_rightDoor.transform.position.x - f_doorMovementTime * Time.deltaTime, _rightDoor.transform.position.y, _rightDoor.transform.position.z);
        }

        else {
            ToDoorStandby();
        }

    }

    private void CloseDoor()
    {
        if (b_isOpen)
        {
            _leftDoor.transform.position = new Vector3(_leftDoor.transform.position.x - f_doorMovementTime * Time.deltaTime, _leftDoor.transform.position.y, _leftDoor.transform.position.z);
            _rightDoor.transform.position = new Vector3(_rightDoor.transform.position.x + f_doorMovementTime * Time.deltaTime, _rightDoor.transform.position.y, _rightDoor.transform.position.z);

            if (_leftDoor.transform.position.x <= f_originalPosX &&
                _rightDoor.transform.position.x >= f_originalPosX)
            {
                _leftDoor.transform.position = new Vector3(f_originalPosX, _leftDoor.transform.position.y, _leftDoor.transform.position.z);
                _rightDoor.transform.position = new Vector3(f_originalPosX, _rightDoor.transform.position.y, _rightDoor.transform.position.z);
                b_isOpen = false;
                ToDoorStandby();
            }
        }

        else {
            ToDoorStandby();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        b_isOpen = true;
        ToDoorStandby();
    }
}
