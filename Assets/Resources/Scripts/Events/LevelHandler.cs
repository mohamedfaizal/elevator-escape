﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/****************************************************************
 Level Handler
 Usage:
 Tracks what the current state of the game the player is in, through
 enum. Attach this script onto an empty GameObject.

 Description:
 Tracks what the current state of the game the player is in

 Ng Jun Guo (Benny)
 ng_junguo901@hotmail.com/ bennyng@viziofly.com
 +65-81020141
 ****************************************************************/

public class LevelHandler : MonoBehaviour {
    public AudioSource _BGMPlayer;
    public AudioSource _voicePlayer;
    public InputBehaviour _leftController;
    public InputBehaviour _rightController;
    public Image _splashPageBackground;
    public GameObject _cameraContainer;

    void Awake() {
        AudioManager.GetInstance().Initialize(_voicePlayer, _BGMPlayer);
        ScriptFlowManager.GetInstance().Initialize();
    }

    // Use this for initialization
    void Start() {
        StartCoroutine(LevelManager.GetInstance().LoadLevel("Splashpage", false));
    }

    // Update is called once per frame
    void Update() {
    }

    public void Restart() {
        _splashPageBackground.gameObject.SetActive(true);
        StartCoroutine(LevelManager.GetInstance().LoadLevel("Splashpage", true));
    }

    public bool ControllerPressed() {
        _cameraContainer.transform.position = new Vector3(0f, 0f, 0f);
        return (_leftController.TouchPadUp() || _rightController.TouchPadUp());
    }
}
